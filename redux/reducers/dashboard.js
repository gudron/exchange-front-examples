import {
  assetRateMachine,
  selectedCurrencyRateMachine,
  expectedPriceMachine,
  singlePriceMachine,
  sparklinesMachine,
  volumesMachine,
  chartMachine,
} from '~/redux/machines/dashboard';

import {
  openModal,
  closeModal,
  selectAmountPercent,
  clearAmountPercent,
  setClickedRow,
  clearClickedRow,
  storeAssetsReturns,
  storeAssets,
  changePairBalance,
  changeSingleBalance,
  setWhitelisted,
  showSplash,
  hideSplash,
  changeMainTab,
  changeBalancesTab,
  changeMarketsTab,
  storeSparklines,
  changeBalancesSearch,
  storeShowcaseAssets,
  storeVolumes,
  changeChartFilter,
  storeChartData,
  storeHasBalancesChanges,
  clickTrade,
  closeTrade,
  setHideSmallAmounts,
} from '../actions/dashboard';

const defaultState = {
  modalType: null,
  modalParams: null,
  selectedPercent: null,
  rowClicked: null,
  pricesReturns: {},
  assetsIds: [],
  assets: null,
  whitelisted: false,
  sortBy: 'total',
  sortDirection: 'down',
  splashed: false,
  mainTab: 'balances',
  balancesSearch: '',
  balancesTab: 'all',
  balancesSortBy: 'total',
  balancesSortDirection: 'down',
  marketsTab: 'all',
  marketsSortBy: 'name',
  marketsSortDirection: 'down',
  sparklines: {},
  showcaseAssetsIds: [],
  showcaseAssets: null,
  volumes: {},
  chartFilter: 'month',
  chart: null,
  chartCurrency: null,
  hasBalancesChanges: null,
  clickedTradeOnAsset: null,
  hideSmallAmounts: false,
};

export default {
  machines: [
    assetRateMachine,
    selectedCurrencyRateMachine,
    expectedPriceMachine,
    singlePriceMachine,
    sparklinesMachine,
    volumesMachine,
    chartMachine,
  ],
  reducer: {
    [openModal]: (state, { modalType, modalParams }) => ({
      ...state,
      modalType,
      modalParams,
    }),
    [closeModal]: (state) => ({
      ...state,
      modalType: null,
      modalParams: null,
    }),
    [selectAmountPercent]: (state, { percent }) => ({
      ...state,
      selectedPercent: percent,
    }),
    [clearAmountPercent]: (state) => ({
      ...state,
      selectedPercent: null,
    }),
    [setClickedRow]: (state, rowClicked) => ({
      ...state,
      rowClicked,
    }),
    [clearClickedRow]: (state) => ({
      ...state,
      rowClicked: null,
    }),
    [storeAssetsReturns]: (state, pricesReturns) => ({
      ...state,
      pricesReturns,
    }),
    [storeAssets]: (state, { assetsIds, assets }) => ({
      ...state,
      assetsIds,
      assets,
    }),
    [changeSingleBalance]: (state, { balance, asset }) => ({
      ...state,
      assets: {
        ...state.assets,
        [asset]: {
          ...state.assets[asset],
          balance,
        },
      },
    }),
    [changePairBalance]: (state, { asset1, balance1, asset2, balance2 }) => ({
      ...state,
      assets: {
        ...state.assets,
        [asset1]: {
          ...state.assets[asset1],
          balance: balance1,
        },
        [asset2]: {
          ...state.assets[asset2],
          balance: balance2,
        },
      },
    }),
    [setWhitelisted]: (state, whitelisted) => ({
      ...state,
      whitelisted,
    }),
    [showSplash]: (state) => ({
      ...state,
      splashed: true,
    }),
    [hideSplash]: (state) => ({
      ...state,
      splashed: false,
    }),
    [changeMainTab]: (state, mainTab) => ({
      ...state,
      mainTab,
    }),
    [changeBalancesTab]: (state, { tab, sortBy, sortDirection }) => ({
      ...state,
      balancesTab: tab,
      balancesSortBy: sortBy,
      balancesSortDirection: sortDirection,
    }),
    [changeMarketsTab]: (state, { tab, sortBy, sortDirection }) => ({
      ...state,
      marketsTab: tab,
      marketsSortBy: sortBy,
      marketsSortDirection: sortDirection,
    }),
    [storeSparklines]: (state, sparklines) => ({
      ...state,
      sparklines,
    }),
    [storeVolumes]: (state, volumes) => ({
      ...state,
      volumes,
    }),
    [changeBalancesSearch]: (state, balancesSearch) => ({
      ...state,
      balancesSearch,
    }),
    [storeShowcaseAssets]: (state, { assetsIds, assets }) => ({
      ...state,
      showcaseAssetsIds: assetsIds,
      showcaseAssets: assets,
    }),
    [changeChartFilter]: (state, chartFilter) => ({
      ...state,
      chartFilter,
    }),
    [storeChartData]: (state, { data, currency }) => ({
      ...state,
      chart: data,
      chartCurrency: currency,
    }),
    [storeHasBalancesChanges]: (state, hasBalancesChanges) => ({
      ...state,
      hasBalancesChanges,
    }),
    [clickTrade]: (state, clickedTradeOnAsset) => ({
      ...state,
      clickedTradeOnAsset,
    }),
    [closeTrade]: (state) => ({
      ...state,
      clickedTradeOnAsset: null,
    }),
    [setHideSmallAmounts]: (state, value) => ({
      ...state,
      hideSmallAmounts: value,
    }),
  },
  defaultState,
};
