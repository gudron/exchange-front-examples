import { takeLatest } from 'redux-saga/effects';

import {
  changeCurrency,
} from '../actions/app';

import cookies from 'js-cookie';
import mixpanel from '~/services/mixpanel';


function handleChangeCurrency(action) {
  const currency = action.payload;

  mixpanel.get().track('Display Currency', { currency });

  cookies.set('display-currency', currency, { expires: 7 });
}

export default function* () {
  yield takeLatest(changeCurrency, handleChangeCurrency);
}
