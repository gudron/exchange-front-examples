import { call, fork, cancelled, select, takeLatest, take, cancel, put, delay } from 'redux-saga/effects';
import { BigNumber } from 'bignumber.js';

import {
  expectedPriceMachine,
  singlePriceMachine,
  sparklinesMachine,
  volumesMachine,
  chartMachine,
} from '~/redux/machines/dashboard';

import {
  openBuy,
  openSell,
  openDeposit,
  openWithdraw,
} from '~/redux/actions/operations';

import {
  changeCurrency,
} from '~/redux/actions/app';

import {
  CRYPTO_TYPE,
  CARD_TYPE,
} from '~/redux/reducers/deposit';

import {
  changeType as changeDepositType,
} from '~/redux/actions/deposit';

import {
  changeType as changeWithdrawType,
} from '~/redux/actions/withdraw';

import { needBalance } from '~/redux/actions/balance';

import { assetsSelector, assetsListSelector } from '~/redux/selectors/assetsList';

import {
  needSinglePrice,
  startListenBalanceChange,
  stopListenBalanceChange,
  changeSingleBalance,
  changePairBalance,
  storeSparklines,
  changeSort,
  changeSubTab,
  changeBalancesTab,
  changeMarketsTab,
  clickOnTable,
  storeVolumes,
  storeChartData,
  changeChartFilter,
  startWaitForChangeCurrency,
  stopWaitForChangeCurrency,
  clickTrade,
} from '~/redux/actions/dashboard';

import { openSocketChannel } from '~/services/socketChannel';

import Request from '~/services/Request';

import { FORM_NAME as BUY_FROM_NAME } from '~/components/buy/BuyForm';
import { FORM_NAME as SELL_FROM_NAME } from '~/components/sell/SellForm';

import { BUY_AMOUNT_FIELD, BUY_CURRENCY_FIELD } from '~/validators/buyForm';
import { SELL_AMOUNT_FIELD, SELL_CURRENCY_FIELD } from '~/validators/sellForm';

const URLS = {
  generateExpectedPriceUrl: (method) => {
    return `/currency/expectedprice/${ method }`;
  },
  assets: '/vbapi/assets',
  assetsPrice: '/vbapi/assetsPrice',
  price: '/vbapi/price',
  sparklines: '/vbapi/sparklines',
  volumes: '/vbapi/volumes',
  chart: '/vbapi/chart',
};

const formMethodMap = {
  [BUY_FROM_NAME]: 'buy',
  [SELL_FROM_NAME]: 'sell',
};

const formFromFieldMap = {
  [BUY_FROM_NAME]: BUY_CURRENCY_FIELD,
  [SELL_FROM_NAME]: SELL_CURRENCY_FIELD,
};

const formAmountFieldMap = {
  [BUY_FROM_NAME]: BUY_AMOUNT_FIELD,
  [SELL_FROM_NAME]: SELL_AMOUNT_FIELD,
};

const BUY_EVENT = 'buy';
const SELL_EVENT = 'sell';
const DEPOSIT_EVENT = 'deposit';
const WITHDRAW_EVENT = 'withdraw';

const BALANCE_EVENTS_TO_LISTEN = [BUY_EVENT, SELL_EVENT, DEPOSIT_EVENT, WITHDRAW_EVENT];

function* waitForNeedSinglePrice() {
  yield takeLatest(needSinglePrice, debouncingSinglePrice);
}

function* debouncingSinglePrice(action) {
  yield delay(500);

  yield call(singlePriceMachine.putRun(action.payload));
}

function* expectedPriceHandler(machine, action) {
  const formName = action.payload;

  const values = yield select(state => state.forms[formName].values);
  const neededCurrency = yield select(state => state.operations.assetId);
  const fromFieldName = formFromFieldMap[formName];
  const amountFieldName = formAmountFieldMap[formName];

  // TODO: prevalidate!
  if (values[amountFieldName] === null || values[amountFieldName] === '' || values[amountFieldName] === undefined) {
    return;
  }

  const amount = parseFloat(values[amountFieldName]);

  const fromCurrency = values[fromFieldName];

  const url = URLS.generateExpectedPriceUrl(formMethodMap[formName]);

  yield call(machine.transitionTo('inProgress'));

  const requestSource = Request.getSource();

  try {
    const response = yield call([
      Request,
      Request.sendPost,
    ],
    url,
    {
      from: neededCurrency,
      to: fromCurrency,
      amount,
    },
    { cancelToken: requestSource.token }
    );

    yield call(machine.transitionTo('success', { amount: response.result, currency: fromCurrency }));
  } catch (error) {
    yield call(machine.transitionTo('error', error.error));
  } finally {
    if (yield cancelled()) {
      requestSource.cancel();
    }
  }
}

function* singlePriceHandler(machine, action) {
  const formName = action.payload;

  const values = yield select(state => state.forms[formName].values);
  const neededCurrency = yield select(state => state.operations.assetId);

  const fromFieldName = formFromFieldMap[formName];

  const fromCurrency = values[fromFieldName];

  yield call(machine.transitionTo('inProgress'));

  const requestSource = Request.getSource();

  try {
    const response = yield call([
      Request,
      Request.sendPost,
    ],
      URLS.price,
      {
        from: neededCurrency,
        to: fromCurrency,
      },
      { cancelToken: requestSource.token }
    );

    yield call(machine.transitionTo('success', { ...response.data, currency: fromCurrency }));
  } catch (error) {
    console.log('error', error);
    yield call(machine.transitionTo('error', error.error));
  } finally {
    if (yield cancelled()) {
      requestSource.cancel();
    }
  }
}

function* sparklinesHandler(machine) {
  yield call(machine.transitionTo('inProgress'));

  const requestSource = Request.getSource();

  try {
    const response = yield call([
      Request,
      Request.sendGet,
    ],
      URLS.sparklines,
      null,
      { cancelToken: requestSource.token }
    );

    yield put(storeSparklines(response.data));
    yield call(machine.transitionTo('success'));
  } catch (error) {
    console.log('error', error);
    yield call(machine.transitionTo('error', error.error));
  } finally {
    if (yield cancelled()) {
      requestSource.cancel();
    }
  }
}

function* volumesHandler(machine) {
  yield call(machine.transitionTo('inProgress'));

  const requestSource = Request.getSource();

  try {
    const response = yield call([
      Request,
      Request.sendGet,
    ],
    URLS.volumes,
    null,
    { cancelToken: requestSource.token }
    );

    yield put(storeVolumes(response.data));
    yield call(machine.transitionTo('success'));
  } catch (error) {
    console.log('error', error);
    yield call(machine.transitionTo('error', error.error));
  } finally {
    if (yield cancelled()) {
      requestSource.cancel();
    }
  }
}

function* chartHandler(machine) {
  yield call(machine.transitionTo('inProgress'));

  const requestSource = Request.getSource();

  const chartFilter = yield select(state => state.dashboard.chartFilter);
  const displayCurrency = yield select(state => state.app.currency);

  try {
    const response = yield call([
      Request,
      Request.sendPost,
    ],
    URLS.chart,
    {
      period: chartFilter,
      displayCurrency,
    },
    { cancelToken: requestSource.token }
    );

    yield put(storeChartData({ data: response.data, currency: displayCurrency }));
    yield call(machine.transitionTo('success'));
  } catch (error) {
    console.log('error', error);
    yield call(machine.transitionTo('error', error.error));
    yield put(storeChartData({ data: null, currency: null }));
  } finally {
    if (yield cancelled()) {
      requestSource.cancel();
    }
  }
}

function* processDeposit(event) {
  const { asset, value } = event;
  const assets = yield select(assetsSelector);
  const currentBalance = new BigNumber(assets[asset].balance);
  const resultBalance = currentBalance.plus(value);

  yield put(changeSingleBalance({ asset, balance: resultBalance.toString() }));
}

function* processWithdraw(event) {
  const { asset, value } = event;
  const assets = yield select(assetsSelector);
  const currentBalance = new BigNumber(assets[asset].balance);
  const resultBalance = currentBalance.minus(value);

  yield put(changeSingleBalance({ asset, balance: resultBalance.toString() }));
}

function* processExchange(event) {
  const { baseAsset, baseAssetValue, quoteAsset, quoteAssetValue } = event;

  const assets = yield select(assetsSelector);

  const asset1Balance = new BigNumber(assets[baseAsset].balance);
  const resultAsset1Balance = asset1Balance.plus(baseAssetValue);

  const asset2Balance = new BigNumber(assets[quoteAsset].balance);
  const resultAsset2Balance = asset2Balance.plus(quoteAssetValue);

  yield put(changePairBalance({
    asset1: baseAsset,
    balance1: resultAsset1Balance,
    asset2: quoteAsset,
    balance2: resultAsset2Balance,
  }));
}

function* processEvents(channel) {
  while (true) {
    const event = yield take(channel);

    switch (event.type) {
      case BUY_EVENT:
      case SELL_EVENT:
        yield fork(processExchange, event);
        yield put(needBalance());
        break;
      case DEPOSIT_EVENT:
        yield fork(processDeposit, event);
        yield put(needBalance());
        break;
      case WITHDRAW_EVENT:
        yield fork(processWithdraw, event);
        yield put(needBalance());
        break;
    }
  }
}

function* waitForStartListenBalanceChange() {
  while (yield take(startListenBalanceChange)) {
    const channel = yield call(openSocketChannel, BALANCE_EVENTS_TO_LISTEN);
    const eventListener = yield fork(processEvents, channel);

    yield take(stopListenBalanceChange);
    yield cancel(eventListener);
    channel.close();
  }
}

function* waitForChangeSort() {
  let payload;

  while ({ payload } = yield take(changeSort)) {
    const mainTab = yield select(state => state.dashboard.mainTab);
    const currentTab = yield select(state => state.dashboard[`${ mainTab }Tab`]);
    const currentSortBy = yield select(state => state.dashboard[`${ mainTab }SortBy`]);
    const currentSortDirection = yield select(state => state.dashboard[`${ mainTab }SortDirection`]);

    const newSortDirection = currentSortBy === payload
      ? (currentSortDirection === 'down' ? 'up' : 'down')
      : 'down';

    switch (mainTab) {
      case 'balances':
        yield put(changeBalancesTab({
          tab: currentTab,
          sortBy: payload,
          sortDirection: newSortDirection,
        }));
        break;
      case 'markets':
        yield put(changeMarketsTab({
          tab: currentTab,
          sortBy: payload,
          sortDirection: newSortDirection,
        }));
        break;
    }
  }
}

function* waitForChangeSubTab() {
  let payload;

  while ({ payload } = yield take(changeSubTab)) {
    const mainTab = yield select(state => state.dashboard.mainTab);
    let sortBy;
    let sortDirection;

    switch (mainTab) {
      case 'balances':
        sortBy = 'total';
        sortDirection = 'down';

        switch (payload) {
          case 'gainers':
            sortBy = 'change';
            sortDirection = 'down';
            break;
          case 'losers':
            sortBy = 'change';
            sortDirection = 'up';
            break;
        }

        yield put(changeBalancesTab({
          tab: payload,
          sortBy: sortBy,
          sortDirection: sortDirection,
        }));
        break;
      case 'markets':
        sortBy = 'name';
        sortDirection = 'down';

        switch (payload) {
          case 'gainers':
            sortBy = 'change';
            sortDirection = 'down';
            break;
          case 'losers':
            sortBy = 'change';
            sortDirection = 'up';
            break;
        }

        yield put(changeMarketsTab({
          tab: payload,
          sortBy,
          sortDirection,
        }));
        break;
    }
  }
}

function* waitForClickOnTable() {
  let payload;

  while ({ payload } = yield take(clickOnTable)) {
    const assets = yield select(assetsListSelector);
    const neededAsset = assets.find(item => item.name === payload.symbol);

    if (payload.action === 'deposit' && !neededAsset.canDeposit) {
      continue;
    }

    if (payload.action === 'withdraw' && !neededAsset.canWithdraw) {
      continue;
    }

    if (payload.action === 'trade' && (!neededAsset.isShowTradeButton || !neededAsset.canTrade || !neededAsset.needKycForTrade)) {
      continue;
    }

    switch (payload.action) {
      case 'trade':
        yield put(clickTrade(payload.symbol));
        break;
      case 'deposit':
        switch (payload.symbol) {
          case 'EUR':
            yield put(changeDepositType(CARD_TYPE));
            yield put(openDeposit());
            break;
          default:
            yield put(changeDepositType(CRYPTO_TYPE));
            yield put(openDeposit(payload.symbol));
        }
        break;
      case 'withdraw':
        switch (payload.symbol) {
          case 'EUR': // TODO: future will be card
          default:
            yield put(changeDepositType(CRYPTO_TYPE));
            yield put(openWithdraw(payload.symbol));
        }
        break;
    }
  }
}

function* waitForChangeCurrency() {
  while (yield take(changeCurrency)) {
    chartMachine.cancelTask();
    yield call(chartMachine.putRun());
  }
}

function* waitForStartWaitForChangeCurrency() {
  while (yield take(startWaitForChangeCurrency)) {
    const task = yield fork(waitForChangeCurrency);

    yield take(stopWaitForChangeCurrency);
    task.cancel();
  }
}

function* waitForChangeChartFilter() {
  while (yield take(changeChartFilter)) {
    chartMachine.cancelTask();
    yield call(chartMachine.putRun());
  }
}

export default function* () {
  yield fork(expectedPriceMachine.makeSaga(expectedPriceHandler));
  yield fork(singlePriceMachine.makeSaga(singlePriceHandler));
  yield fork(sparklinesMachine.makeSaga(sparklinesHandler));
  yield fork(volumesMachine.makeSaga(volumesHandler));
  yield fork(chartMachine.makeSaga(chartHandler));

  yield fork(waitForNeedSinglePrice);
  yield fork(waitForStartListenBalanceChange);
  yield fork(waitForChangeSubTab);
  yield fork(waitForChangeSort);
  yield fork(waitForClickOnTable);
  yield fork(waitForChangeChartFilter);
  yield fork(waitForStartWaitForChangeCurrency);
}
