import { normalize } from 'normalizr';

import { receiveUserData } from '~/redux/panel/actions/panel';
import { silentChangeCurrency } from '~/redux/actions/app';
import { getUser, makeRequest, getDisplayCurrency } from '~/redux/preparers/common';
import redirect from '~/services/redirect';

import { storeDigits } from '~/redux/actions/digits';
import { hasPortfolioAccess } from '~/server/services/accessPreparer';
import { isCardRestrictedUser } from '~/redux/selectors/deposit';
import { setDefaultCardStep as setDefaultDepositCardStep, setDefaultCryptoStep as setDefaultDepositCryptoStep } from '~/redux/actions/deposit';
import { setDefaultCryptoStep as setDefaultWithdrawCryptoStep } from '~/redux/actions/withdraw';
import { storeWallets } from '~/redux/actions/wallets';
import { storeAssetsReturns, setWhitelisted, storeAssets, storeShowcaseAssets, storeHasBalancesChanges } from '~/redux/actions/dashboard';
import { walletsSchema } from '~/redux/schemas/wallets';
import { digitsSchema } from '~/redux/schemas/digits';
import { vbAssetsSchema } from '~/redux/schemas/vbAssets';
import { showcaseAssetsSchema } from '~/redux/schemas/showcaseAssets';

function prepareDigits(digits) {
  const normalized = normalize(digits, digitsSchema);

  return normalized.entities.digits;
}

function prepareWallets(wallets) {
  const normalized = normalize(wallets, walletsSchema);

  return {
    wallets: normalized.entities.wallets,
    walletsIds: normalized.result,
  };
}

function prepareAssets(assets) {
  const normalized = normalize(assets, vbAssetsSchema);

  return {
    assets: normalized.entities.vbAssets,
    assetsIds: normalized.result,
  };
}

function prepareShowcaseAssets(assets) {
  const normalized = normalize(assets, showcaseAssetsSchema);

  return {
    assets: normalized.entities.showcaseAssets,
    assetsIds: normalized.result,
  };
}

export async function getDashboardInitialProps({ store, req, res, isServer }) {
  const userData = await getUser(req);

  if (!hasPortfolioAccess(userData)) {
    return redirect(res, '/login');
  }

  const displayCurrency = getDisplayCurrency(req);
  store.dispatch(silentChangeCurrency(displayCurrency));

  store.dispatch(receiveUserData(userData));

  let wallets;
  let assetsReturns;
  let whitelisted;
  let digits;
  let vbAssets;
  let showcaseAssets;
  let hasBalanceChanges;

  if (isServer) {
    wallets = req.wallets;
    assetsReturns = req.assetsReturns;
    whitelisted = req.whitelisted;
    digits = req.digits;
    vbAssets = req.vbAssets;
    showcaseAssets = req.showcaseAssets;
    hasBalanceChanges = req.hasBalanceChanges;
  } else {
    const data = await makeRequest('/_jsdata/dashboard');

    wallets = data.wallets;
    assetsReturns = data.assetsReturns;
    whitelisted = data.whitelisted;
    digits = data.digits;
    vbAssets = data.vbAssets;
    showcaseAssets = data.showcaseAssets;
    hasBalanceChanges = data.hasBalanceChanges;
  }

  if (isCardRestrictedUser(userData)) {
    store.dispatch(setDefaultDepositCryptoStep());
    store.dispatch(setDefaultWithdrawCryptoStep());
  } else {
    store.dispatch(setDefaultDepositCardStep());
    store.dispatch(setDefaultWithdrawCryptoStep());
  }

  // for not logged in it will be null
  if (digits) {
    store.dispatch(storeDigits(prepareDigits(digits)));
  }

  store.dispatch(storeAssets(prepareAssets(vbAssets)));
  store.dispatch(storeShowcaseAssets(prepareShowcaseAssets(showcaseAssets)));
  store.dispatch(storeWallets(prepareWallets(wallets)));
  store.dispatch(storeAssetsReturns(assetsReturns));
  store.dispatch(setWhitelisted(whitelisted));
  store.dispatch(storeHasBalancesChanges(hasBalanceChanges));

  return {
    isLoggedIn: !!userData,
  };
}
