import { createAction } from 'redux-act';

export const changeCurrency = createAction('app.changeCurrency');
export const silentChangeCurrency = createAction('app.silentChangeCurrency');
