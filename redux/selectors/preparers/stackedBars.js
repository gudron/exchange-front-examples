import _ from 'lodash';

import { getHighchartDefaultOptions } from '~/helpers/chart';

export default function (data) {
  if (!data.series) {
    return null;
  }

  const settings = _.merge(getHighchartDefaultOptions(), {
    chart: {
      type: 'column',
    },
    xAxis: {
      categories: data.categories,
      tickmarkPlacement: 'on',
    },
    colors: ['#f5d764', '#b7f99a', '#00adb8', '#005cb8'],
    yAxis: {
      min: 0,
      max: 100,
      title: {
        text: null,
      },
      stackLabels: {
        enabled: false,
        style: {
          fontWeight: 'bold',
          color: 'gray',
        },
      },
      labels: {
        format: '{value}%',
      },
    },
    legend: {
      verticalAlign: 'top',
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}%',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
      },
      series: {
        borderWidth: 0,
      },
    },
    series: data.series,
  }, data.chartSettings);

  return settings;
}
