import Highcharts from 'highcharts';
import _ from 'lodash';

import { navigator } from "./common";
import { getHighchartDefaultOptions } from '~/helpers/chart';

import { formatCurrency } from '~/helpers/money';

const CHART_COLORS = ['#005cb8', '#697684'];

const XAXIS_LABELS_STYLE = {
  color: '#979797',
};

export default function prepareChartSettings(selectedData, allData, currency, fundDescription) {
  const currencyUpper = _.toUpper(currency);

  let defaultSettings = {
    chart: {
      zoomType: 'x',
    },
    legend: {
      enabled: false,
    },
    colors: CHART_COLORS,
    xAxis: {
      labels: {
        style: XAXIS_LABELS_STYLE,
      },
      dateTimeLabelFormats: {
        month: '%b \'%y',
        day: '%e %b \'%y',
      },
      lineColor: '#999',
      crosshair: true,
      minRange: 3600 * 1000 * 24,
    },
    scrollbar: {
      liveRedraw: false,
    },
    yAxis:
      {
        labels: {
          formatter: function () {
            return this.value + ' ' + currencyUpper;
          },
          style: {
            color: '#005cb8',
          },
        },
        opposite: false,
        startOnTick: false,
        endOnTick: false,
        alignTicks: false,
        title: {
          text: null,
        },
        showLastLabel: true,
      },
    plotOptions: {
      spline: {
        dataGrouping: {
          enabled: false,
        },
      },
      column: {
        dataGrouping: {
          enabled: false,
        },
      },
    },
    tooltip: {
      formatter: function () {
        var s = '<b>';
        s += Highcharts.dateFormat('%e %b \'%y %H:%M', this.x) + '</b>';
        this.points.forEach(function (point) {
          s += '<br/>';
          s += '1 ' + fundDescription.token + ' = ' + formatCurrency(point.y, currency) + ' ' + currencyUpper;
        });
        return s;
      },
      shared: true,
    },
    navigator: {
      ...navigator,
      adaptToUpdatedData: false,
      series: {
        data: allData,
      },
    },
    series: [{
      name: '',
      data: selectedData,
      type: 'spline',
      tooltip: {
        valueDecimals: 2,
      },
      dataGrouping: {
        enabled: false,
      },
    }],
  };

  return _.merge(getHighchartDefaultOptions(), defaultSettings);
}
