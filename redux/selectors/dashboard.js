import { createSelector } from 'reselect';
import BigNumber from 'bignumber.js';
import _ from 'lodash';

import { selectedCurrencyRateMachine } from '~/redux/machines/dashboard';

import { assetsIdsSelector, assetsSelector } from './assetsList';
import { kycPassed } from '~/redux/selectors/user';

import { CRYPTO_TYPE } from '~/redux/reducers/deposit';
import { HIDDEN_ASSETS } from '~/server/helpers/hiddenAssets';

// import Joi from 'joi-browser';

const FIAT_ASSETS = ['EUR', 'USD'];

const FIAT_WITHDRAW_SETTINGS = {
  EUR: {
    minAmount: 100,
    fee: 25,
  },
  USD: {
    minAmount: 110,
    fee: 30,
  },
};

const modalType = state => state.operations.modalType;
const modalParams = state => state.operations.assetId; // todo: rename
const depositTypeSelector = state => state.deposit.depositType;
const pricesReturnsSelector = state => state.dashboard.pricesReturns;
const displayCurrencySelector = state => state.app.currency;
const clickedTradeOnAssetSelector = state => state.dashboard.clickedTradeOnAsset;
const mainTabSelector = state => state.dashboard.mainTab;
const marketsTabSelector = state => state.dashboard.marketsTab;
const balancesTabSelector = state => state.dashboard.balancesTab;
const whitelisted = state => state.dashboard.whitelisted;

export const modalDataSelector = createSelector(
  modalType,
  modalParams,
  assetsSelector,
  (modalType, modalParams, assets) => {
    const asset = assets[modalParams];

    return asset;
  }
);

export const modalBalanceSelector = createSelector(
  modalType, modalDataSelector,
  (modalType, modalData) => {
    if (modalType !== 'sell') {
      return null;
    }

    return new BigNumber(modalData.balance);
  }
);

export const modalBalanceInDisplayCurrencySelector = createSelector(
  modalBalanceSelector,
  selectedCurrencyRateMachine.getStateSelector(),
  selectedCurrencyRateMachine.getDataSelector(),
  (modalBalance, convertedRateState, convertedRateData) => {
    if (modalBalance === null) {
      return null;
    }

    if (convertedRateState !== 'success') {
      return null;
    }

    const rate = new BigNumber(convertedRateData.Value.toString());
    const currency = convertedRateData.To;
    const convertedAmount = modalBalance.multipliedBy(rate);

    return {
      amount: convertedAmount,
      currency,
    };
  }
);

export const convertableAssetsSelector = createSelector(
  modalParams,
  modalType,
  assetsSelector,
  (modalParams, modalType, assets) => {
    if (!modalParams) {
      return null;
    }

    if (modalType !== 'buy' && modalType !== 'sell') {
      return null;
    }


    const openedAssetId = modalParams;
    console.log('assets', assets, openedAssetId)

    const asset = assets[openedAssetId];

    const convertableAssets = asset.cash.slice(0);

    return convertableAssets.map(assetId => assets[assetId]).filter(asset => {
      switch (modalType) {
        case 'buy':
          return asset.canSell;
        case 'sell':
          return asset.canBuy;
      }
    });
  }
);

const depositAssetsSelector = createSelector(
  modalType,
  assetsIdsSelector,
  assetsSelector,
  depositTypeSelector,
  kycPassed,
  whitelisted,
  (modalType, assetsIds, assets, depositType, kycPassed, whitelisted) => {
    if (modalType !== 'deposit') {
      return null;
    }

    if (depositType !== CRYPTO_TYPE) {
      return null;
    }

    if (!modalParams) {
      return null;
    }

    const canDepositIds = assetsIds
      .filter(assetID => whitelisted ? true : !HIDDEN_ASSETS.includes(assets[assetID].routingKey))
      .filter(assetId => {
        if (!kycPassed && assets[assetId].kycOnly === true) {
          return false;
        }

        return assets[assetId].canDeposit === true;
      });

    return canDepositIds.map(assetId => assets[assetId]);
  }
);

const withdrawAssetsSelector = createSelector(
  modalType,
  assetsIdsSelector,
  assetsSelector,
  whitelisted,
  (modalType, assetsIds, assets, whitelisted) => {
    if (modalType !== 'withdraw') {
      return null;
    }

    if (!modalParams) {
      return null;
    }

    const canWithdrawIds = assetsIds
      .filter(assetId => whitelisted ? true : !HIDDEN_ASSETS.includes(assets[assetId].routingKey))
      .filter(assetId => {
        return assets[assetId].canWithdraw === true;
      });

    return canWithdrawIds.map(assetId => assets[assetId]);
  }
);

export function optionsGenerate(assets, prices, displayCurrency) {
  return assets.map(asset => {
    const pricesByRoutingKey = _.get(prices, [asset.routingKey, displayCurrency, 'tokenPrice']);
    const priceByName = _.get(prices, [_.toLower(asset.name), displayCurrency, 'tokenPrice']);
    const price = new BigNumber(pricesByRoutingKey || priceByName);
    const balance = new BigNumber(asset.balance);

    return {
      value: asset.name,
      label: asset.displayName,
      balance: balance,
      currency: asset.name,
      convertedAmount: price === null ? null : price.multipliedBy(balance),
      convertedCurrency: displayCurrency,
      isPricesLoading: false,
    };
  });

}

export function walletsBalancesGenerate(assets) {

  const balances = assets.reduce((map, item)=>{
    const balance = new BigNumber(item.balance);
    map[item.name] = balance;

    return map;
  }, {});

  return balances;
}

const walletsBalancesGenerator = (assets) => {
  if (assets === null) {
    return null;
  }

  return walletsBalancesGenerate(assets);
};

export const walletsBalancesSelector = createSelector(
  convertableAssetsSelector,
  walletsBalancesGenerator,
);


const optionsGenerator = (assets, pricesReturns, displayCurrency) => {
  if (assets === null) {
    return null;
  }

  const prices = pricesReturns;
  const pricesCurrency = displayCurrency;

  return optionsGenerate(assets, prices, pricesCurrency);
};

export const convertableAssetsOptionsSelector = createSelector(
  convertableAssetsSelector,
  pricesReturnsSelector,
  displayCurrencySelector,
  optionsGenerator,
);

export const depositCryptoAssetsOptionsSelector = createSelector(
  depositAssetsSelector,
  pricesReturnsSelector,
  displayCurrencySelector,
  (assets, pricesReturns, displayCurrency) => {
    if (assets === null) {
      return null;
    }

    const cryptoAssets = assets.filter(asset => FIAT_ASSETS.indexOf(asset.name) === -1);

    return optionsGenerator(cryptoAssets, pricesReturns, displayCurrency);
  },
);

export const withdrawCryptoAssetsOptionsSelector = createSelector(
  withdrawAssetsSelector,
  pricesReturnsSelector,
  displayCurrencySelector,
  (assets, pricesReturns, displayCurrency) => {
    if (assets === null) {
      return null;
    }

    const cryptoAssets = assets.filter(asset => FIAT_ASSETS.indexOf(asset.name) === -1);

    return optionsGenerator(cryptoAssets, pricesReturns, displayCurrency);
  },
);

export const withdrawFiatAssetsOptionsSelector = createSelector(
  withdrawAssetsSelector,
  (assets) => {
    if (assets === null) {
      return null;
    }

    const assetsIds = [];
    const assetsItems = {};

    assets.filter(asset => {
      return FIAT_ASSETS.indexOf(asset.name) !== -1;
    }).forEach(asset => {

      assetsIds.push(asset.name);

      assetsItems[asset.name] = {
        name: asset.name,
        displayName: asset.displayName,
        balance: new BigNumber(asset.balance),
        description: asset.description,
        ...FIAT_WITHDRAW_SETTINGS[asset.name],
      };
    });

    return {
      assetsIds,
      assets: assetsItems,
    };
  }
);

export const subTabSelector = createSelector(
  mainTabSelector, balancesTabSelector, marketsTabSelector,
  (mainTab, balancesTab, marketsTab) => {
    if (mainTab === 'balances') {
      return balancesTab;
    }

    return marketsTab;
  }
);

export const tradeOptionsSelector = createSelector(
  clickedTradeOnAssetSelector, assetsSelector,
  (clickedTradeOnAsset, assets) => {
    if (clickedTradeOnAsset === null) {
      return null;
    }

    const asset = assets[clickedTradeOnAsset];

    return asset.cash.map(item => (
      { value: item, label: _.toUpper(item) }
    ));

  }
);
