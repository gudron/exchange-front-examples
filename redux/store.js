import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import cookie from 'js-cookie';
import { connect } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import createSagaMiddleware from 'redux-saga';

import { ls } from '~/services/localStorage';

import rootReducer from './rootReducer';
import rootSaga from './sagas';

export function configureStore(initialState = {}) {
  const isProduction = process.env.NODE_ENV === 'production';

  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [
    applyMiddleware(sagaMiddleware),
  ];

  if (!ls.initialized && typeof window !== 'undefined') {
    ls.setStorage(window.localStorage);
  }

  if (!isProduction && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }

  const store = createStore(
    combineReducers(rootReducer),
    initialState,
    compose.apply(null, middlewares)
  );

  store.sagaTask = sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('./rootReducer', () => {
      const reducers = require('./rootReducer').default;
      console.log('Reducer updated');
      return store.replaceReducer(combineReducers(reducers));
    });

    module.hot.accept('./sagas', () => {
      const newSagas = require('./sagas').default;

      store.sagaTask.toPromise().then(() => {
        store.sagaTask = sagaMiddleware.run(newSagas);
        console.log('Saga updated');
      });

      store.sagaTask.cancel();
    });
  }

  return store;
}

export function appWithRedux(App) {
  return withRedux(configureStore)(withReduxSaga(App));
}

export function pageWithRedux(Page) {
  return connect()(Page);
}
