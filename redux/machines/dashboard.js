import StateMachine from 'services/StateMachine';

export const assetRateMachine = new StateMachine('assetRateMachine');
export const selectedCurrencyRateMachine = new StateMachine('selectedCurrencyRate');
export const expectedPriceMachine = new StateMachine('expectedPrice');
export const singlePriceMachine = new StateMachine('singlePrice');
export const sparklinesMachine = new StateMachine('sparklines');
export const volumesMachine = new StateMachine('volumes');
export const chartMachine = new StateMachine('chart');
