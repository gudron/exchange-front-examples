import prepareRootReducer from '~/services/prepareRootReducer';

import panel from './panel/reducers/panel';
import * as reducers from './reducers';

export default prepareRootReducer({
  panel,
  ...reducers,
});
