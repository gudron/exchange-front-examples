export function preparePercent(percent) {
  const formatted = Math.floor(percent) - percent === 0 ? percent.toFixed(1) : percent.toFixed(2);

  return `${ percent > 0 ? ('+' + formatted) : formatted }%`;
}
