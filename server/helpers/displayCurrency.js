export const CURRENCY_OPTIONS = [
  { label: 'USD', value: 'usd' },
  { label: 'EUR', value: 'eur' },
  { label: 'BTC', value: 'btc' },
  { label: 'ETH', value: 'eth' },
];

export const DEFAULT_CURRENCY = 'usd';

export const parseCurrency = function(currency, defaultCurrency) {
  const foundCurrency = CURRENCY_OPTIONS.filter(function(item) {
    return item.value === currency;
  }).length > 0;

  if (foundCurrency) {
    return currency;
  }

  return defaultCurrency;
}
