import { parseCurrency } from './displayCurrency';
const DEFAULT_DISPLAY_CURRENCY = 'eth';

export function getAssetCurrency(cookieDisplayCurrency, assetDescription) {
  const displayCurrency = parseCurrency(
    cookieDisplayCurrency,
    assetDescription.defaultDisplayCurrency || DEFAULT_DISPLAY_CURRENCY
  );

  return displayCurrency;
}
