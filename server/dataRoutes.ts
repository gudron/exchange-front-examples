import { Router, Response } from 'express';
import logger from './logger';

import {
  getAssetContracts,
  getUserSymbolsList,
  getAssetWithBookAndSubscriptionStatus,
} from './services/asset';

import {
  errorResponse,
} from './services/request';

import { hasPartnershipAccess } from './services/accessPreparer';
import { getShowcaseData, getShowcaseTabData } from './services/products';
import { getAbi } from './services/abi';
import { getPartnershipData } from './services/partnership';
import { getCurrencyRate } from './services/currencyRate';
import { getDashboardData } from './services/dashboard';

import { Promise } from 'es6-promise';

const contractAddressParamExp = /^([a-zA-Z0-9]+).json$/;
const currencyParamExp = /^([a-z0-9]{3})$/;
const defaultContract = {};

function isInvestor(req): boolean {
  return req.userData && req.userData.kycPassed && req.userData.roles.indexOf('investor') !== -1;
}

export const setupDataRoutes = (router: Router): void => {s

  router.use('/_jsdata/dashboard', (req, res): void => {
    getDashboardData(req).then((result): void => {
      res.json(result);
    }).catch((error): Response => {
      if (error.code === 403) {
        return errorResponse(res, 'forbidden', 403);
      }

      return errorResponse(res, 'Internal server error', 500);
    });
  });
};
