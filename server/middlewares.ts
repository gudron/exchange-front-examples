import { Express } from 'express';

import productsMiddleware from './middlewares/products';
import termsOfSaleMiddleware from './middlewares/termsOfSale';
import { partnershipMiddleware } from './middlewares/partnership';
import dashboardMiddleware from './middlewares/dashboard';
import { portfolioAccessMiddleware, partnershipAccessMiddleware } from './middlewares/account';
import walletsMiddleware from './middlewares/wallets';
import showcaseAssetMiddleware from './middlewares/showcaseAsset';
import confirmationMiddleware from './middlewares/confirmation';

export const setupMiddlewares = (server: Express) => {
  // todo: extend it on all routes when backend deployed
  // router.use(walletMiddleware);
  server.get('/products', productsMiddleware);

  server.get('/showcase/:assetId/*', showcaseAssetMiddleware);

  server.get('/dashboard*', portfolioAccessMiddleware);
  server.get('/dashboard', dashboardMiddleware);

  server.get('/partnership', partnershipAccessMiddleware);
  server.get('/partnership', partnershipMiddleware);

  server.get('/terms-of-sale', termsOfSaleMiddleware);

  server.get('/settings/wallets', walletsMiddleware);

  server.get('/confirmation/:hash', confirmationMiddleware);
};
