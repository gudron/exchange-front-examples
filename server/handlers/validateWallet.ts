import WAValidator from "multicoin-address-validator"

export default (req, res): void => {
    if (!req.query || !req.query.address || !req.query.symbol) {
        res.statusCode = 403;
        res.json({ 
            error: "address, symbol is required",
            valid: false 
        });
        return;
    }

    const address = req.query.address;
    const symbol = req.query.symbol;
    const chainnetType = req.query.testnet === 'true' ? 'testnet' : 'prod'; 

    try {
        const validAddr = WAValidator.validate(address, symbol, chainnetType)

        res.statusCode = 200;
        res.json({ valid: validAddr });

        return;
      } catch (err) {

        res.statusCode = 500;
        res.json({ 
            error: "internal error",
            valid: false 
        });
      }


};
