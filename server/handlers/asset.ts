import { app } from '../app';
import logger from '../logger';

import { getAssetWithBookAndSubscriptionStatus, BookWithSubsriptionStatus } from '../services/asset';

export default ({ req, res, route, query }, next): void => {
  if (!query || !query.assetId) {
    next({ req, res, route, query });
    return;
  }

  if (query.assetId.indexOf('unknown') === 0) {
    res.redirect('/');
    return;
  }

  getAssetWithBookAndSubscriptionStatus(req, query.assetId).then((result: BookWithSubsriptionStatus): void => {
    req.asset = result.asset;
    req.redemptions = result.redemptions;
    req.wallets = result.wallets;
    req.vbAsset = result.vbAsset;
    req.assetsReturns = result.assetsReturns;
    req.digits = result.digits;

    next({ req, res, route, query });
  }).catch((error): Promise<void> => {
    if (error.code === 400) {
      res.redirect('/');
      return;
    }

    logger.log('error', {
      file: 'handler/asset',
      error: error.message,
      userData: req.userData,
      assetId: query.assetId,
      code: error.code,
    });

    res.statusCode = 500;
    return app.renderError(null, req, res, "/_error", req.query);
  });
};
