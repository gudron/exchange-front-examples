import axios from 'axios';

class AxiosError extends Error {
  public status?: number;
  public description?: string;

  constructor(message?: string) {
    super(message); // 'Error' breaks prototype chain here
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }
}

export function setupAxios() {
  const handleAxiosError = (err) => {
    if (err.response) {
      const customError = new AxiosError(err.response.statusText || 'Internal server error');
      customError.status = err.response.status || 500;
      customError.description = err.response.data ? err.response.data.message : null;
      throw customError;
    }
    throw new Error(err);
  };

  // axios.default.interceptors
  axios.interceptors.response.use((r) => r, handleAxiosError);
}
