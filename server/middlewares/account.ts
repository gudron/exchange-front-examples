import { hasPartnershipAccess, hasPortfolioAccess } from '../services/accessPreparer';

export const generateRoleMiddleware = function(role) {
  return function(req, res, next) {
    if (!req.isUserLoggedIn) {
      res.redirect(303, '/');
      return;
    }

    if (!req.userData.kycPassed) {
      res.redirect(303, '/');
      return;
    }

    if (req.userData.roles.indexOf(role) !== -1) {
      next();
      return;
    }

    res.redirect(303, '/');
  };
};

export const portfolioAccessMiddleware = function (req, res, next) {
  if (!hasPortfolioAccess(req.userData)) {
    res.redirect(303, '/login');
    return;
  }

  next();
}

export const partnershipAccessMiddleware = function(req, res, next) {
  if (!hasPartnershipAccess(req.userData)) {
    res.redirect(303, '/login');
    return;
  }

  next();
};
