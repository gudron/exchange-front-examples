import env from '../env';

export default function (req, _res, next) {
  req.appConfig = env;

  next();
}
