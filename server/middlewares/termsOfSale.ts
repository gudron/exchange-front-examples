import path from 'path';
import fs from 'fs';
import { app } from '../app';
import { getTermsOfSale } from '../services/termsOfSale';
import { getByUserData } from '../services/asset';
import Mustache from 'mustache';
import countries from 'svg-country-flags/countries.json';
import logger from '../logger';
import _ from 'lodash';

const underlyingAssetsBySymbol = {
  'EEM': 'MSCI EEM',
  'SPY': 'SPDR SP500',
  'XMI': 'Shares of Xiaomi corporation',
  '1DV': 'portfolio of cryptocurrencies as defined in the Token Supplement',
  'DDR': 'portfolio of cryptocurrencies as defined in the Token Supplement',
  'T20': 'portfolio of cryptocurrencies as defined in the Token Supplement',
};

let cachedFile = '';

fs.readFile(path.resolve(__dirname, 'terms-of-sale.html'), (err, data) => {
  if (err) {
    logger.log('error', 'Can not cache terms-of-sale', err);
    return;
  }

  logger.log('info', 'cached terms-of-sale');
  cachedFile = data.toString();
});

interface TermsOfSaleData {
  userId: number;
  actsAs: string;
  asPerson: boolean;
  asCompany: boolean;
  usPerson: boolean;
  date: Date;
  purchaseToken: string;
  purchaseTokenSp: string;
  purchaseCurrency: string;
  underlyingAsset: string;
  residenceCountryLabel: string;
  companyCountryLabel: string;
  residenceCountry: string;
  companyCountry: string;
}

interface TermsOfSale {
  data: TermsOfSaleData;
}

export default (req, res) => {
  if (!req.isUserLoggedIn) {
    res.statusCode = 403;
    return app.renderError(null, req, res, '/_error', req.query);
  }

  if (!req.query || isNaN(parseInt(req.query.ledgerId, 10)) || isNaN(parseInt(req.query.termsOfSaleId, 10))) {
    res.statusCode = 400;
    return app.renderError(null, req, res, '/_error', req.query);
  }

  const ledgerId = parseInt(req.query.ledgerId, 10);
  const termsOfSaleId = parseInt(req.query.termsOfSaleId, 10);

  getTermsOfSale(req, ledgerId, termsOfSaleId).then((data: TermsOfSale) => {
    res.set('Content-Type', 'text/html; charset=UTF-8');
    const info = data.data;
    info.userId = _.get(info, 'userId');
    const actAs = _.get(info, 'actAs');
    info.asPerson = actAs === 'person';
    info.asCompany = actAs === 'company';
    info.usPerson = _.get(req.userData, 'USPerson');
    info.date = new Date(info.date);

    const assetId = info.purchaseToken;

    info.purchaseToken = info.purchaseToken ? _.toUpper(info.purchaseToken) : info.purchaseToken;

    const asset = getByUserData(req.userData, assetId);

    if (asset.isStrategy) {
      info.purchaseTokenSp = 'CIT';
    } else {
      switch (info.purchaseToken) {
        case 'XMI':
        case 'SPY':
        case 'EEM':
          info.purchaseTokenSp = 'ExT';
          break;

        case 'PMR':
          info.purchaseTokenSp = 'PM';
          break;

        default:
          res.statusCode = 500;
          logger.log('error', `Token ${ info.purchaseToken } not have map for Terms of Sale`);
          return app.renderError(null, req, res, '/_error', req.query);
      }
    }

    info.purchaseCurrency = info.purchaseCurrency ? _.toUpper(info.purchaseCurrency) : info.purchaseCurrency;
    info.underlyingAsset = underlyingAssetsBySymbol[info.purchaseToken];
    info.residenceCountryLabel = countries[info.residenceCountry];
    info.companyCountryLabel = countries[info.companyCountry];

    res.send(Mustache.render(cachedFile, info));
  }).catch((error) => {
    res.statusCode = 500;
    console.log('error', error);
    return app.renderError(null, req, res, '/_error', req.query);
  });
};
