import axios from 'axios';

import { app } from '../app';

import { urlResolve } from '../urlResolve';
import logger from '../logger';

const AUTH_URL = urlResolve('/auth/userInfo');

export default function(req, res, next) {
  if (req.cookies.blackmooncrypto) {

    // TODO: cache requests for some time
    axios.request({
      url: AUTH_URL,
      method: "get",
      headers: req.headers,
    }).then(function (response) {
      if (!response.data.success) {
        next();
        return;
      }

      req.userData = response.data.data;
      req.isUserLoggedIn = !!req.userData;
      req.isSuspended = !!req.userData && req.userData.isSuspended;
      next();
    }).catch(function (error) {
      if (error.status === 401) {
        req.userData = null;
        next();
        return;
      }

      res.statusCode = 500;
      logger.log('error', {
        file: 'authMiddleware',
        error: error.message,
        cookie: req.cookies.blackmooncrypto,
        url: AUTH_URL,
        code: error.code,
        status: error.status,
      });
      return app.renderError(null, req, res, "/_error", req.query);
    });
  } else {
    req.isUserLoggedIn = false;
    next();
  }
}
