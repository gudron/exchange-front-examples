import { getUserSymbolsList } from '../services/asset';

const redirectMap = {
  sp500: 'spy',
  iShares: 'eem',
  primeMeridian: 'pmr',
  gargoyleGroup: 'ghv',
};

function isHasAccessToAsset(req) {
  if (!req.params.assetId) {
    throw new Error('You should add `assetId` param to request');
  }

  // opened all assets
  return true;
}

export default function (req, res, next) {
  if (!isHasAccessToAsset(req)) {
    res.redirect(303, '/');
    return;
  }

  const { assetId } = req.params;

  if (redirectMap[assetId] !== undefined) {
    res.redirect(301, req.url.replace(assetId, redirectMap[assetId]));
    return;
  }

  getUserSymbolsList(req).then((symbols: Array<string>) => {
    if (symbols.indexOf(assetId) === -1) {
      res.redirect(303, '/');
      return;
    }

    next();
  }).catch(() => {
    res.statusCode = 500;
    return res.json({ error: 'Internal server error' });
  });
}
