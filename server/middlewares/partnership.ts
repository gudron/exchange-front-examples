import { app } from '../app';
import { getPartnershipData } from '../services/partnership';

export function partnershipMiddleware(req, res, next) {
  getPartnershipData(req).then((data) => {
    req.wallets = data.wallets;
    req.dashboard = data.dashboard;
    next();
  }).catch(() => {
    res.statusCode = 500;
    return app.renderError(null, req, res, '/_error', req.query);
  });
}
