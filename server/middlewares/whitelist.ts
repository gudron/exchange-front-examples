import { app } from '../app';

import { WHITELIST } from '../services/whitelist';

export const whitelistMiddleware = (req, res, next) => {
  if (!req.isUserLoggedIn || !req.userData || WHITELIST.indexOf(req.userData.email) === -1) {
    res.statusCode = 404;
    return app.renderError(null, req, res, '/_error', req.query);
  }

  next();
};
