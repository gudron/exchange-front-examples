import { getAssetContracts } from '../services/asset';

export default function(req, _res, next) {
  req.assetsContracts = getAssetContracts();

  next();
}
