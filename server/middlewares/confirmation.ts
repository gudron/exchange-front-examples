
import {
  getUserSymbolsList,
} from '../services/asset';

export default (req: any, _res: any, next: any) => {
  const assetFromCookie = req.cookies.bm_asset;

  req.neededRedirect = null;

  if (!assetFromCookie) {
    next();
    return;
  }

  getUserSymbolsList(req).then((symbols: string[]) => {
    if (symbols.indexOf(assetFromCookie) !== -1) {
      req.neededRedirect = `/showcase/${ assetFromCookie }/about`;
    }

    next();
  }).catch(() => {

    next();
  });

};
