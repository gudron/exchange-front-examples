import { app } from '../app';
import { getDashboardData } from '../services/dashboard';

export default (req, res, next): void => {
  getDashboardData(req).then((data): void => {
    req.assetsReturns = data.assetsReturns;
    req.wallets = data.wallets;
    req.whitelisted = data.whitelisted;
    req.digits = data.digits;
    req.vbAssets = data.vbAssets;
    req.showcaseAssets = data.showcaseAssets;
    req.hasBalanceChanges = data.hasBalanceChanges;

    next();
  }).catch((): any => {
    res.statusCode = 500;
    return app.renderError(null, req, res, '/_error', req.query);
  });
};
