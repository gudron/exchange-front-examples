import { app } from '../app';
import { getShowcaseData } from '../services/products';

export default function(req, res, next): void {
  getShowcaseData(req).then((showcaseData): void => {
    req.productParams = showcaseData.productParams;
    req.symbolsByTabs = showcaseData.symbolsByTabs;
    req.assetsReturns = showcaseData.assetsReturns;

    next();
  }).catch((): any => {
    res.statusCode = 500;
    return app.renderError(null, req, res, '/_error', req.query);
  });
}
