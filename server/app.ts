import next from 'next';

import { dev } from './vars';

export const app = next({
  dir: '.', // base directory where everything is, could move to src later
  dev,
});
