import * as winston from 'winston';
const { combine, timestamp, printf, colorize } = winston.format;
import { dev } from './vars';

function devFormat() {
  const formatMessage = info => `${ info.timestamp } [${ info.level }]: ${ JSON.stringify(info.message) }`;
  const formatError = info =>  `${ info.timestamp } [${ info.level }]: ${ info.message }\n\n${ info.stack }\n`;
  const format = info => info instanceof Error ? formatError(info.message) : formatMessage(info);
  return combine(timestamp(), colorize(), printf(format));
}

const myFormat = printf(info => {
  return `${ info.timestamp } [${ info.level }]: ${ JSON.stringify(info.message) }`;
});

export default winston.createLogger({
  transports: [
    new winston.transports.Console(),
  ],
  format: dev ? devFormat() : combine(
    timestamp(),
    myFormat
  ),
});
