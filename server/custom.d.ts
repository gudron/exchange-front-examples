export as namespace Express;

export interface Request {
  isUserLoggedIn: boolean;
  isSuspended: boolean;
  userData ?: {
    canBuy: boolean;
    canSell: boolean;
    email: string;
  };
}
