import routes from './routes';

import assetHandler from './handlers/asset';
// import { Next} from 'next';

export const setupAndGetHandler = (app) => {
  const renderApp = ({ req, res, route, query }) => {
    app.render(req, res, route.page, query);
  };

  const handle = routes.getRequestHandler(app, function (params) {
    const { route } = params;

    switch (route.name) {
      case 'showcase/about':
      case 'showcase/news':
        assetHandler(params, renderApp);
        break;
      default:
        renderApp(params);
    }
  });

  return handle;

}
