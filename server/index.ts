import express from 'express';
import url from 'url';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import { Promise } from 'es6-promise';

import { app } from './app';
import { env, dev } from './vars';

import { setupProxy } from './proxy';
import { setupAxios } from './axios';
import { setupAndGetHandler } from './handlers';
import logger from './logger';

import { cacheAssetsInfo, cacheAssetContracts } from './services/asset';
import { cacheABIs } from './services/abi';

import authMiddleware from './middlewares/auth';
import appConfigMiddleware from './middlewares/appConfig';
import { setupDataRoutes } from './dataRoutes';
import { setupMiddlewares } from './middlewares';
import { setupRedirects } from './redirects';
import { sourcemapsForSentryOnly } from './sourcemapsForSentryOnly';
import validateWalletHandler from './handlers/validateWallet';

import { resolve } from 'path';

const port = parseInt(process.env.PORT, 10) || 3000;
const staticDir = dev ? resolve(__dirname, '..', '.next', 'static') : resolve(__dirname, '..', '..', '.next', 'static');

setupAxios();

// const handle = app.getRequestHandler();
const router = express.Router();

cacheAssetContracts()
  .then((): Promise<string[]> => cacheABIs())
  .then((): void => {
    // prepare lite/full info about asset before start
    cacheAssetsInfo();

    app
      .prepare()
      .then((): void => {
        const { Sentry } = require('./sentry')({ release: app.buildId, sentryDsn: process.env.SENTRY_PUBLIC_DSN });

        const server = express();

        server.use(Sentry.Handlers.requestHandler());

        if (dev) {
          setupProxy(server);
        }

        server.get('/', (_req, res): void => {
          res.redirect(301, '/login');
        });

        server.use(bodyParser.json());
        server.use(cookieParser());
        server.get('/validate-wallet', validateWalletHandler);
        server.use('/static', express.static(staticDir));

        // TODO: check if its not call on _jsdata and /static
        router.use(authMiddleware);
        server.use(router);

        setupRedirects(router);

        setupDataRoutes(router);

        router.use(appConfigMiddleware);

        setupMiddlewares(server);

        const handle = setupAndGetHandler(app);

        server.get(/\.map$/, sourcemapsForSentryOnly(process.env.SENTRY_TOKEN, dev));

        server.all('*', (req, res): void => {
          //   // https://github.com/zeit/next.js/issues/1189
          const parsedUrl = url.parse(req.url, true);
          const { pathname, query } = parsedUrl;

          if (pathname.length > 1 && pathname.slice(-1) === '/') {
            app.render(req, res, pathname.slice(0, -1), query);
          } else {
            handle(req, res, parsedUrl);
          }
        });

        server.use(Sentry.Handlers.errorHandler());

        server.listen(port, (err): void => {
          if (err) {
            throw err;
          }
          logger.log('info', `> Ready on port ${ port } [${ env }]`);
        });
      })
      .catch((err): void => {
        console.log('err', err);
        logger.log('error', err);
      });
  });
