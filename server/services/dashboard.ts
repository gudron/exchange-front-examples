import { getAllAssetsReturns } from './assetReturn';
import { getUserWallets } from './wallets';
import { getDigits } from './digits';
import { getShowcase } from './products';
import logger from '../logger';
import { inWhitelist } from './whitelist';
import { resolveByKeys } from '../promiseHelpers';
import { getVbAssets } from './vbassets';
import { getHasBalanceChanges } from './balanceChanges';
import { Promise } from 'es6-promise';

export interface DashboardData {
  assetsReturns: any;
  wallets: any;
  whitelisted: boolean;
  digits: any;
  vbAssets: any;
  showcaseAssets: any;
  hasBalanceChanges: boolean;
}

export const getDashboardData = (req: any): Promise<DashboardData> => {
  return new Promise((resolve, reject): void => {
    const needResolveByKey = {
      assetsReturns: getAllAssetsReturns(req),
      wallets: getUserWallets(req),
      digits: getDigits(req),
      vbAssets: getVbAssets(req),
      showcaseAssets: getShowcase(req),
      hasBalanceChanges: getHasBalanceChanges(req),
    };

    resolveByKeys(needResolveByKey).then((result): void => {
      resolve({
        assetsReturns: result.assetsReturns,
        wallets: result.wallets,
        whitelisted: inWhitelist(req),
        digits: result.digits,
        vbAssets: result.vbAssets,
        showcaseAssets: result.showcaseAssets,
        hasBalanceChanges: result.hasBalanceChanges,
      });
    }).catch((error): void => {
      console.log('error', error);
      logger.log('error', {
        file: 'dashboard',
        error,
      });

      reject('Catched error');
    });
  });
};
