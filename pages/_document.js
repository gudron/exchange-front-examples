import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import sprite from 'svg-sprite-loader/runtime/sprite.build';

const chat = {
  __html: `<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="756a41b2-63ba-48fc-a10d-c89b7c41bc02";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>`,
};

export default class MainDocument extends Document {
  static getInitialProps({ renderPage, req }) {
    const { html, head, errorHtml, chunks } = renderPage();

    const spriteContent = { __html: sprite.stringify() };
    const appConfig = req ? req.appConfig : {};
    return { html, head, errorHtml, chunks, spriteContent, appConfig };
  }

  render() {
    return (
      <html lang="en" prefix="og: http://ogp.me/ns#">
        <Head>
          <link rel="stylesheet" type="text/css" href="/static/next/nprogress.css" />
        </Head>
        <body>
          <div className="sprites" dangerouslySetInnerHTML={ this.props.spriteContent } />
          <Main />
          <div id="app-config" data-config={ JSON.stringify(this.props.appConfig) } ></div>
          <NextScript />
          <div dangerouslySetInnerHTML={ chat }></div>
        </body>
      </html>
    )
  }
}
