import React, { Component } from 'react';

import LayoutWithSidebar from '~/components/LayoutWithSidebar';
import Dashboard from '~/components/dashboard/Dashboard';

import { pageWithRedux } from '~/redux/store';
import { getDashboardInitialProps } from '~/redux/preparers/dashboard';
import mixpanel from '~/services/mixpanel';

class DashboardPage extends Component {
  static getInitialProps = getDashboardInitialProps;

  componentDidMount() {
    mixpanel.get().track('Page view', {
      page: 'Dashboard',
    });
  }

  render() {
    return (
      <LayoutWithSidebar page="dashboard" title="Dashboard" showCurrencySelect showBalance black>
        <Dashboard />
      </LayoutWithSidebar>
    );
  }
}

export default pageWithRedux(DashboardPage);
