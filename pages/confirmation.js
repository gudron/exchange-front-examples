import React, { Component } from 'react';

import Layout from '~/components/Layout';
import Confirmation from '~/components/screens/Confirmation';

import { pageWithRedux } from '~/redux/store';
import { getConfirmationInitialProps } from '~/redux/preparers/confirmation';

import './Confirmation.sass';

class ConfirmationPage extends Component {
  static getInitialProps = getConfirmationInitialProps;

  render() {
    return (
      <Layout page="confirmation" title="Confirmation">
        <main className="login-app-screen white-content">
          <div className="login-app-screen-content">
            <Confirmation
              confirmationHash={ this.props.confirmationHash }
              neededRedirect={ this.props.neededRedirect }
            />
          </div>
        </main>
      </Layout>
    )
  }
}

export default pageWithRedux(ConfirmationPage);
