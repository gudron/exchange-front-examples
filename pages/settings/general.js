import React, { Component } from 'react';

import LayoutWithSidebar from '~/components/LayoutWithSidebar';
import SettingsLayout from '~/components/screens/settings/SettingsLayout';
import GeneralSettings from '~/components/screens/settings/GeneralSettings';
import { pageWithRedux } from '~/redux/store';
import { getSettingsInitialProps } from '~/redux/preparers/settings';

class GeneralSettingsScreen extends Component {
  static getInitialProps = getSettingsInitialProps;

  render() {
    return (
      <LayoutWithSidebar page="settings/general" title="General settings" header="Settings">
        <SettingsLayout>
          <GeneralSettings />
        </SettingsLayout>
      </LayoutWithSidebar>
    );
  }
}

export default pageWithRedux(GeneralSettingsScreen);
