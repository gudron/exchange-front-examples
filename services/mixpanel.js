import cookies from 'js-cookie';

const URL = '/tracksrv/track';
const DISTINCT_ID_COOKIE_NAME = 'bm_distinctid';

let updateLock = false; // update for first load page
let identifiedUserId = null;

function getRandomString() {
  return (Math.random()).toString(16).substring(2);
}

function generateDistinctId() {
  return 'bm-' + getRandomString() + '-' + getRandomString();
}

function getDistinctId() {
  if (identifiedUserId !== null) {
    return identifiedUserId;
  }

  let distinctId = cookies.get(DISTINCT_ID_COOKIE_NAME);
  let forceUpdate = false;

  if (!distinctId) {
    forceUpdate = true;
    distinctId = generateDistinctId();
  }

  if (!updateLock || forceUpdate) {
    cookies.set(DISTINCT_ID_COOKIE_NAME, distinctId, { expires: 365 });
    updateLock = true;
  }

  return distinctId;
}

function trackRequest(data) {
  // Default options are marked with *
  return fetch(URL, {
    body: JSON.stringify(data), // must match 'Content-Type' header
    credentials: 'same-origin', // include, same-origin, *omit
    headers: {
      'content-type': 'application/json',
    },
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
  }); // parses response to JSON
}
// for user-blocked
class MixpanelMock {
  track(event, properties = {}) {
    const data = {
      distinctId: getDistinctId().toString(),
      event: {
        event,
        properties: {
          ...properties,
          Adblock: true,
        },
      },
    };

    trackRequest({
      data: window.btoa(JSON.stringify(data)),
    });
  }
  identify(id) {
    identifiedUserId = id;
  }
  alias() { }
  people = {
    set: () => { },
  }
  get_distinct_id() {
    return getDistinctId();
  }
  reset() {
    cookies.remove(DISTINCT_ID_COOKIE_NAME);
  }
}

const mixpanelMock = new MixpanelMock();

const getMixpanel = () => {
  if (typeof window === 'undefined') {
    // null - for exception if on server side was call (unexpected)
    return null;
  }

  if (!window.mixpanel) {
    return mixpanelMock;
  }

  return window.mixpanel;
};

class MixpanelService {
  get() {
    return getMixpanel();
  }
}

export default new MixpanelService();
