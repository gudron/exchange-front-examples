let configCache = null;
let isConfigInited = false;

// for panel
const initConfig = function (config) {
  isConfigInited = true;
  configCache = config;
};

const getAppConfig = function () {
  if (isConfigInited) {
    if (typeof configCache === 'function') {
      return configCache();
      //test
    }
    return configCache;
  }

  if (configCache === null) {
    if (process.browser === true) {
      const appConfigElement = document.getElementById('app-config');
      configCache = JSON.parse(appConfigElement.dataset.config);
    } else {
      configCache = {
        ENV_VARIABLE: process.env.ENV_VARIABLE,

      };
    }
  }

  return configCache;
};

const getConfigValue = function(property) {
  if (isConfigInited) {
    if (typeof configCache === 'function') {
      return configCache(property);
      //test
    }
    return configCache[property];
  }

  return getAppConfig()[property];
};

export { getAppConfig, initConfig, getConfigValue };
