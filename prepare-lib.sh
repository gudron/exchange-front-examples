#! /bin/bash

rm -rf lib/js

BABEL_ENV=panel babel panel --out-dir lib/js/panel
BABEL_ENV=panel babel --extensions ".tsx,.js" components --out-dir lib/js/components
BABEL_ENV=panel babel services --out-dir lib/js/services
BABEL_ENV=panel babel redux --out-dir lib/js/redux
BABEL_ENV=panel babel helpers --out-dir lib/js/helpers
BABEL_ENV=panel babel server/services --out-dir lib/js/server/services
BABEL_ENV=panel babel server/helpers --out-dir lib/js/server/helpers
cp services/usStates.json lib/js/services/usStates.json
cp redux/selectors/preparers/stackedColumnsColors.json lib/js/redux/selectors/preparers/stackedColumnsColors.json
