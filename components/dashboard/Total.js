import React, { Component } from 'react';
import PropTypes from 'prop-types';

import BoldIntegerPart from '~/components/BoldIntegerPart';
import RectSpinner from '~/components/RectSpinner';
import Button from '~/components/Button';
import { formatCurrency, prepareCurrencyTitle } from '~/helpers/money';
import Svg from '~/components/Svg';
import WithdrawSvg from '~/svg/withdraw.svg';
import DepositSvg from '~/svg/deposit.svg';
import TooltipButton from '~/components/TooltipButton';
import VerifyContent from '~/components/tooltips/VerifyContent';

export default class Total extends Component {
  static propTypes = {
    balance: PropTypes.object,
    balanceRequestStatus: PropTypes.string,
    onDepositClick: PropTypes.func.isRequired,
    onWithdrawClick: PropTypes.func.isRequired,
    kycPassed: PropTypes.bool.isRequired,
    canDepositWithdraw: PropTypes.string.isRequired,
  }

  prevent(e) {
    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    const { balance, balanceRequestStatus } = this.props;

    return (
      <div className="dashboard-total">
        <div className="dashboard-total__column">
          <div className="dashboard-total__hint">
            Portfolio Value
          </div>
          {
            balanceRequestStatus == 'inProgress' &&
            <div className="dashboard-total__spinner">
              <RectSpinner small className="white" />
            </div>
          }
          {
            balance && balanceRequestStatus == 'success' &&
            <div className="dashboard-total__amount">
              <b>{ prepareCurrencyTitle(balance.currency) }</b> <BoldIntegerPart value={ formatCurrency(balance.balance, balance.currency, true) } />
            </div>
          }
        </div>

        { this.props.canDepositWithdraw &&
          <div className="dashboard-total__column dashboard-total__column--buttons">
            <Button
              className="button deposit-button"
              onClick={ this.props.onDepositClick }
            ><Svg src={ DepositSvg } />Deposit</Button>
            <Button
              onClick={ this.props.onWithdrawClick }
              className="button white-border"
            ><Svg src={ WithdrawSvg } />Withdraw</Button>
          </div>
        }
      </div>
    );
  }
}
