import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import RectSpinner from '~/components/RectSpinner';
import NumberAnimate from '~/components/NumberAnimate';
import InfoSvg from '~/components/once-modals/info.svg';
import Link from '~/components/Link';
import ActionCell from '~/components/dashboard/ActionCell';
import Svg from '~/components/Svg';
import SparklineDecimal from '~/components/trade-pairs/SparklineDecimal';
import { formatCurrency, prepareCurrencyTitle } from '~/helpers/money';
import { preparePercent } from '~/server/helpers/percent';
import { isLaptop } from '~/helpers/adaptive';

import ImportSvg from './icons/import_inherit.svg';
import ExportSvg from './icons/export_inherit.svg';
import TradeSvg from './icons/trade_inherit.svg';

class AssetsTableRow extends Component {
  static propTypes = {
    asset: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
    onBuyClick: PropTypes.func.isRequired,
    onSellClick: PropTypes.func.isRequired,
    onRowClick: PropTypes.func.isRequired,
    isClicked: PropTypes.bool,
    convertedInProgress: PropTypes.bool,
    isLock: PropTypes.bool.isRequired,
    displayCurrency: PropTypes.string.isRequired,
  }

  handleBuyClick = () => {
    // TODO: warning hadcode
    if (this.props.asset.name === 'EUR') {
      this.props.onSellClick('USDT');
      return;
    }

    this.props.onBuyClick(this.props.asset.name);
  }

  handleSellClick = () => {
    // TODO: warning hadcode
    if (this.props.asset.name === 'EUR') {
      this.props.onBuyClick('USDT');
      return;
    }

    this.props.onSellClick(this.props.asset.name);
  }

  handleRowClick = (e) => {
    e.preventDefault();

    if (isLaptop() && !this.props.asset.routingKey) {
      return;
    }

    this.props.onRowClick(this.props.asset.routingKey || this.props.asset.name);
  }

  stop(e) {
    e.stopPropagation();
  }

  render() {
    const { asset, isLock } = this.props;

    const assetReturn = asset.change;
    const assetPrice = asset.price;

    const nameContainerContent = (<div className="assets-table__asset-name-container">
      <div>
        <div
          className={ classnames('assets-table__logo', `${ asset.name.toLowerCase() }-color`) }
          data-label={ asset.displayName }
        />
      </div>
      <div className="assets-table__asset-name-column">
        <div className="assets-table__asset-name">
          { asset.displayName }
        </div>
        <div className="assets-table__token">{ asset.description }</div>
      </div>
    </div>);

    return (
      <div
        className={ classnames('assets-table__row', {
          'assets-table__row--clicked': this.props.isClicked,
        }) }
        onClick={ this.handleRowClick }
      >
        <div className={ classnames('assets-table__cell assets-table__cell--info', { 'assets-table__cell--info--has-link': !!asset.routingKey }) }>
          {
            asset.routingKey
              ? <Link route="showcase/about" params={ { assetId: asset.routingKey } } onClick={ this.stop }>
                <Svg src={ InfoSvg } className="assets-table__icon--desktop-hidden" />
                { nameContainerContent }
              </Link>
              : nameContainerContent
          }
        </div>
        <div className="assets-table__cell assets-table__cell--total">
          {
            this.props.convertedInProgress &&
            <RectSpinner inline small />
          }
          {
            !isLock && asset.isPriceLoaded &&
            <span>
              { prepareCurrencyTitle(asset.priceCurrency) } { formatCurrency(asset.total, asset.priceCurrency, true) }
            </span>
          }
          {
            !isLock &&
              <div className="assets-table__cell--subtotal assets-table__cell--tokens">
                { formatCurrency(asset.balance, asset.name, false) }
              </div>
          }
        </div>
        <div className="assets-table__cell assets-table__cell--return">
          {
            !isLock && (assetReturn !== null) && (assetReturn !== 'hidden') && (assetReturn !== 'TBA') &&
            <div
              className={ classnames('assets-table__change', {
                'assets-table__change--positive': assetReturn.isPositive(),
                'assets-table__change--negative': assetReturn.isNegative(),
              }) }
            >
              <NumberAnimate
                number={ assetReturn }
              >
                { preparePercent(assetReturn) }
              </NumberAnimate>
            </div>
          }
          {
            (assetReturn === 'TBA') &&
              <div className="assets-table__change">
                <span>{ 'TBA' }</span>
              </div>
          }
        </div>
        <div className="assets-table__cell assets-table__cell--subtotal assets-table__cell--price">
          {
            !isLock && asset.sparkline &&
              <div><SparklineDecimal width={ 140 } data={ asset.sparkline } /></div>
          }
          {
            asset.isPriceInProgress && <RectSpinner inline small />
          }
          {
            !isLock && assetPrice != null &&
            <div className="assets-table__cell--align-right">
              <NumberAnimate
                number={ assetPrice }
              >
                { prepareCurrencyTitle(asset.priceCurrency) } { formatCurrency(assetPrice, asset.priceCurrency, true) }
              </NumberAnimate>
            </div>
          }
        </div>
        <ActionCell
          className="assets-table__cell--mobile-float-first"
          isDisabled={ !asset.canDeposit }
          symbol={ asset.name }
          action="deposit"
          icon={ ImportSvg }
          title="Deposit"
          isLock={ asset.needKycForDeposit || isLock }
        />
        <ActionCell
          className="assets-table__cell--mobile-float-second"
          isDisabled={ !asset.canWithdraw }
          symbol={ asset.name }
          action="withdraw"
          icon={ ExportSvg }
          title="Withdraw"
          isLock={ asset.needKycForWithdraw || isLock }
        />
        <ActionCell
          className="assets-table__cell--mobile-float-third"
          isDisabled={ !asset.canTrade || !asset.isShowTradeButton }
          symbol={ asset.name }
          action="trade"
          icon={ TradeSvg }
          title="Trade"
          isHidden={ !asset.isShowTradeButton }
          isLock={ asset.needKycForTrade }
        />
      </div>
    );
  }
}

export default AssetsTableRow;
