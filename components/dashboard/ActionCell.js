import React, { useState } from 'react';
import classnames from 'classnames';
import Svg from '~/components/Svg';
import AnimateTooltip from '~/components/tooltips/AnimateTooltip';
import LockSvg from '~/components/dashboard/icons/lock-closed.svg';
import Button from '~/components/Button';
import Link from '~/components/Link';

const commonClassNames = 'assets-table__cell assets-table__cell--button assets-table__cell--mobile-float'

const ActionCell = ({ icon, title, isDisabled, className, symbol, action, isHidden, isLock }) => {
  if (isHidden) {
    return <div className={ classnames(commonClassNames, className) }></div>;
  }


  if (isLock) {
    const [isShow, setIsShow] = useState(false);
    return <div className={ classnames(commonClassNames, className, 'assets-table__cell--lock') } onMouseEnter={ () => { setIsShow(true); } } onMouseLeave={ () => { setIsShow(false); } }>
      <AnimateTooltip
        messageContent={ (
          <div className="verify-for-action-tooltip">
            <div className="text">Verify for { action }</div>

            <Link route="/settings/general?verify"><Button className="button button--new-blue">Verify</Button></Link>
          </div>
        ) }
        isVisible={ isShow }
        usePortal
        containerClassName="animate-tooltip--action-buttons"
      >
        <Svg className="lock-svg" src={ LockSvg } />
        <div className="assets-table__mobile-text">{ title }</div>
      </AnimateTooltip>
    </div>;
  }

  return (
    <div
      className={
        classnames(
          commonClassNames,
          className,
          {
            'assets-table__cell--button--disabled': isDisabled,
          }
        )
      }
      data-symbol={ symbol }
      data-action={ action }
    >
      <Svg src={ icon } />
      <div className="assets-table__mobile-text">{ title }</div>
    </div>
  );
};

export default ActionCell;
