import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Total from '~/components/dashboard/Total';
import AssetsTable from '~/components/dashboard/AssetsTable';
import TradeTabs from '~/components/trade-pairs/TradeTabs';
import TradePairs from '~/components/trade-pairs/TradePairs';
import Switch from '~/components/Switch';

import ShowcaseProduct from '~/components/showcase/ShowcaseProduct';
import Swiper from '~/components/Swiper';
import RectSpinner from '~/components/RectSpinner';
import Chart from '~/components/dashboard/chart/Chart';

import { CARD_SUCCESS_MODAL, CARD_ERROR_MODAL, CARD_WITHDRAW_SUCCESS_MODAL, MOBILE_TRADING_MODAL } from '~/components/once-modals/OnceModal';
import ConfirmDialog from '~/components/ConfirmDialog';

import { showOnceModalModal } from '~/redux/actions/onceModals';

import portfolioMachine from '~/redux/machines/portfolio';
import walletsBalanceMachine from '~/redux/machines/walletsBalance';
import { balanceMachine } from '~/redux/machines/balance';
import assetCurrencyRateMachine from '~/redux/machines/assetCurrencyRate';
import { walletsOptionsSelector } from '~/redux/selectors/wallets';
import { getWalletOptions } from '~/redux/selectors/wallet';
import { tfaEnabled } from '~/redux/selectors/user';
import { withRouter } from 'next/router';
import {
  openBuy,
  openSell,
  closeModal,
  openDeposit,
  openWithdraw,
} from '~/redux/actions/operations';
import { assetsListSelector, plainAssetsSelector } from '~/redux/selectors/assetsList';
import VbOperationsContainer from '~/components/VbOperationsContainer';

import {
  portfolioTotalSelector,
  dashboardAssetsSelector,
  convertedSymbolsAmountSelector,
} from '~/redux/selectors/portfolio';

import {
  startWaitForAssetReturnsChanges,
  stopWaitForAssetReturnsChanges,
} from '~/redux/actions/assetReturns';

import {
  topGrowthSelector,
} from '~/redux/selectors/showcase';

import { kycPassed } from '~/redux/selectors/user';
import Link from '~/components/Link';

import {
  modalBalanceSelector,
  modalDataSelector,
  convertableAssetsOptionsSelector,
  walletsBalancesSelector,
  subTabSelector,
  tradeOptionsSelector,
} from '~/redux/selectors/dashboard';

import {
  getWindowWidth,
  calculateBigSparklineWidth,
} from '~/components/trade-pairs/helpers';

import {
  selectAmountPercent,
  setClickedRow,
  clearClickedRow,
  startWaitForChangeCurrency,
  stopWaitForChangeCurrency,
  startListenBalanceChange,
  stopListenBalanceChange,
  changeSort,
  changeMainTab,
  changeSubTab,
  changeBalancesSearch,
  clickOnTable,
  changeChartFilter,
  closeTrade,
  setHideSmallAmounts,
} from '~/redux/actions/dashboard';
import { sparklinesMachine, volumesMachine, chartMachine } from '~/redux/machines/dashboard';

import { redirectToExchange } from '~/services/exchange';
import { isMobile } from '~/helpers/adaptive';

import mixpanel from '~/services/mixpanel';
import { TABS as SUB_TABS } from '~/components/trade-pairs/helpers';

import './Dashboard.sass';
import './DashboardContainer.sass';

const SWIPER_OPTIONS = {
  slidesPerView: 'auto',
  speed: 300,
  allowTouchMove: true,
  simulateTouch: false,
  autoResize: false,
  containerClass: 'dashboard__swiper-wrap',
  breakpoints: {
    1000: {
      slidesPerGroup: 1,
      allowTouchMove: true,
    },
  },
};

const CHART_STYLES = {
  tickStyle: {
    color: 'rgba(255,255,255,.4)',
    fontSize: '10px',
    fontWeight: 500,
    fill: 'rgba(255,255,255,.4)',
  },
  tickLine: { stroke: '#1F2433' },
  gridStyle: {
    stroke: '#202433',
  },
  receiveLineColor: '#00B865',
  sentLineColor: '#F06E6E',
  mainLineColor: '#0B72D8',
};

const CHART_OPTIONS = [
  { value: 'day', label: '1D' },
  { value: 'week', label: '1W' },
  { value: 'month', label: '1M' },
  { value: 'year', label: '1Y' },
  { value: 'all', label: 'All' },
];

const mapStateToProps = state => ({
  portfolioTotal: portfolioTotalSelector(state),
  dashboardAssets: dashboardAssetsSelector(state),
  convertedAmounts: convertedSymbolsAmountSelector(state),
  userData: state.panel.userData,
  walletsOptions: walletsOptionsSelector(state),
  tfaEnabled: tfaEnabled(state),
  walletOptions: getWalletOptions(state),
  walletBalances: walletsBalancesSelector(state),
  modalType: state.dashboard.modalType,
  modalParams: state.dashboard.modalParams,
  modalBalance: modalBalanceSelector(state),
  rowClicked: state.dashboard.rowClicked,
  ...portfolioMachine.stateToProps(state),
  ...assetCurrencyRateMachine.stateToProps(state),
  ...walletsBalanceMachine.stateToProps(state),
  ...balanceMachine.stateToProps(state),
  ...chartMachine.stateToProps(state),
  assetsList: assetsListSelector(state),
  modalData: modalDataSelector(state),
  convertableAssetsOptions: convertableAssetsOptionsSelector(state),
  balance: state.balance.balance,
  kycPassed: kycPassed(state),
  marketsSortBy: state.dashboard.marketsSortBy,
  marketsSortDirection: state.dashboard.marketsSortDirection,
  balancesSortBy: state.dashboard.balancesSortBy,
  balancesSortDirection: state.dashboard.balancesSortDirection,
  mainTab: state.dashboard.mainTab,
  subTab: subTabSelector(state),
  pricesReturns: state.dashboard.pricesReturns,
  sparklines: state.dashboard.sparklines,
  volumes: state.dashboard.volumes,
  plainAssets: plainAssetsSelector(state),
  balancesSearch: state.dashboard.balancesSearch,
  topGrowth: topGrowthSelector(state),
  chartFilter: state.dashboard.chartFilter,
  chart: state.dashboard.chart,
  chartCurrency: state.dashboard.chartCurrency,
  hasBalancesChanges: state.dashboard.hasBalancesChanges,
  clickedTradeOnAsset: state.dashboard.clickedTradeOnAsset,
  clickedTradeOptions: tradeOptionsSelector(state),
  hideSmallAmounts: state.dashboard.hideSmallAmounts,
});

const mapDispatchToProps = (dispatch) => ({
  openDepositModal: () => {
    mixpanel.get().track('Deposit Start');
    dispatch(openDeposit());
  },
  openWithdrawModal: () => {
    mixpanel.get().track('Withdraw Start');
    dispatch(openWithdraw());
  },
  openBuyModal: (assetId) => {
    mixpanel.get().track('Order Start', {
      'Asset Name': assetId,
      'Order Type': 'Buy',
      'button': 'dashboard',
    });
    dispatch(openBuy(assetId));
  },
  openSellModal: (assetId) => {
    mixpanel.get().track('Order Start', {
      'Asset Name': assetId,
      'Order Type': 'Sell',
      'button': 'dashboard',
    });
    dispatch(openSell(assetId));
  },
  closeModal: () => {
    dispatch(closeModal());
  },
  setClickedRow:(assetId) => {
    dispatch(setClickedRow(assetId));
  },
  clearClickedRow: () => {
    dispatch(clearClickedRow());
  },
  startWaitForChangeCurrency: () => {
    dispatch(startWaitForChangeCurrency());
  },
  stopWaitForChangeCurrency: () => {
    dispatch(stopWaitForChangeCurrency());
  },
  selectAmountPercent: (value) => {
    dispatch(selectAmountPercent(value));
  },
  startListenBalanceChange: () => {
    dispatch(startListenBalanceChange());
  },
  stopListenBalanceChange: () => {
    dispatch(stopListenBalanceChange());
  },
  showSuccess: () => {
    dispatch(showOnceModalModal({
      name: CARD_SUCCESS_MODAL,
    }));
  },
  showError: () => {
    dispatch(showOnceModalModal({
      name: CARD_ERROR_MODAL,
    }));
  },
  showWithdrawSuccess: () => {
    dispatch(showOnceModalModal({
      name: CARD_WITHDRAW_SUCCESS_MODAL,
    }));
  },
  showMobileTrading: () => {
    dispatch(showOnceModalModal({
      name: MOBILE_TRADING_MODAL,
    }));
  },
  changeSort: (fieldName) => {
    dispatch(changeSort(fieldName));
  },
  startWaitForAssetReturnsChanges: () => {
    dispatch(startWaitForAssetReturnsChanges());
  },
  stopWaitForAssetReturnsChanges: () => {
    dispatch(stopWaitForAssetReturnsChanges());
  },
  changeMainTab: (mainTab) => {
    dispatch(changeMainTab(mainTab));
  },
  changeSubTab: (subTab) => {
    dispatch(changeSubTab(subTab));
  },
  needSparklines: () => {
    sparklinesMachine.dispatchRun(dispatch);
  },
  needVolumes: () => {
    volumesMachine.dispatchRun(dispatch);
  },
  needChart: () => {
    chartMachine.dispatchRun(dispatch);
  },
  changeBalancesSearch: (search) => {
    dispatch(changeBalancesSearch(search));
  },
  clickOnTable: (payload) => {
    dispatch(clickOnTable(payload));
  },
  changeChartFilter: (filter) => {
    dispatch(changeChartFilter(filter));
  },
  closeTrade: () => {
    dispatch(closeTrade());
  },
  setHideSmallAmounts: (value) => {
    dispatch(setHideSmallAmounts(value));
  },
});

const successRegexp = /#success$/;
const successWithdrawRegexp = /#success-withdraw$/;
const errorRegexp = /#error/;
const mobileTradingRegexp = /#mobile-trading/;

const MAIN_TABS = [
  { name: 'Balances', value: 'balances' },
  { name: 'Markets', value: 'markets' },
];

class DashboardPage extends Component {
  static propTypes = {
    portfolioTotal: PropTypes.object,
    convertedAmounts: PropTypes.object,
    dashboardAssets: PropTypes.array,
    userData: PropTypes.object,
    walletBalances:  PropTypes.object.isRequired,
    walletsOptions: PropTypes.array.isRequired,
    walletOptions: PropTypes.array.isRequired,
    openDepositModal: PropTypes.func.isRequired,
    openWithdrawModal: PropTypes.func.isRequired,
    modalParams: PropTypes.object,
    modalType: PropTypes.string,
    openBuyModal: PropTypes.func.isRequired,
    openSellModal: PropTypes.func.isRequired,
    modalBalance: PropTypes.object,
    setClickedRow: PropTypes.func.isRequired,
    clearClickedRow: PropTypes.func.isRequired,
    rowClicked: PropTypes.number,
    assetsList: PropTypes.array,
    modalData: PropTypes.object,
    convertableAssetsOptions: PropTypes.array,
    balance: PropTypes.object,
    startWaitForChangeCurrency: PropTypes.func.isRequired,
    stopWaitForChangeCurrency: PropTypes.func.isRequired,
    selectAmountPercent: PropTypes.func.isRequired,
    priceReturns: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
    startListenBalanceChange: PropTypes.func.isRequired,
    stopListenBalanceChange: PropTypes.func.isRequired,
    kycPassed: PropTypes.bool.isRequired,
    showSuccess: PropTypes.func.isRequired,
    showError: PropTypes.func.isRequired,
    balancesSortBy: PropTypes.string.isRequired,
    balancesSortDirection: PropTypes.string.isRequired,
    marketsSortBy: PropTypes.string.isRequired,
    marketsSortDirection: PropTypes.string.isRequired,
    changeSort: PropTypes.func.isRequired,
    showWithdrawSuccess: PropTypes.func.isRequired,
    splashed: PropTypes.bool.isRequired,
    startWaitForAssetReturnsChanges: PropTypes.func.isRequired,
    stopWaitForAssetReturnsChanges: PropTypes.func.isRequired,
    changeMainTab: PropTypes.func.isRequired,
    mainTab: PropTypes.string.isRequired,
    changeSubTab: PropTypes.func.isRequired,
    subTab: PropTypes.string.isRequired,
    needSparklines: PropTypes.func.isRequired,
    needVolumes: PropTypes.func.isRequired,
    balancesSearch: PropTypes.string.isRequired,
    changeBalancesSearch: PropTypes.func.isRequired,
    clickOnTable: PropTypes.func.isRequired,
    topGrowth: PropTypes.array.isRequired,
    changeChartFilter: PropTypes.func.isRequired,
    chartFilter: PropTypes.string.isRequired,
    needChart: PropTypes.func.isRequired,
    chart: PropTypes.array,
    chartCurrency: PropTypes.string,
    hasBalancesChanges: PropTypes.bool.isRequired,
    router: PropTypes.object.isRequired,
    closeTrade: PropTypes.func.isRequired,
    clickedTradeOnAsset: PropTypes.string,
    clickedTradeOptions: PropTypes.array,
    showMobileTrading: PropTypes.func.isRequired,
    setHideSmallAmounts: PropTypes.func.isRequired,
    hideSmallAmounts: PropTypes.bool.isRequired,
  }

  state = {
    bigSparklineWidth: 140,
  }

  getChildContext() {
    return {
      LinkComponent: Link,
    };
  }

  static childContextTypes = {
    LinkComponent: PropTypes.func,
  };

  componentDidMount() {
    this.props.startWaitForAssetReturnsChanges();

    this.props.needSparklines();
    this.props.needVolumes();

    if (this.props.hasBalancesChanges) {
      this.props.needChart();
      this.props.startWaitForChangeCurrency();
    }

    this.props.startListenBalanceChange();

    this.checkIfSuccessNeeded();
    this.checkIfDepositModalNeeded();

    window.addEventListener('resize', this.recalculateSparklines);
    this.recalculateSparklines();
  }

  recalculateSparklines = () => {
    this.setState({
      bigSparklineWidth: calculateBigSparklineWidth(getWindowWidth()),
    });
  }

  checkIfSuccessNeeded() {
    const { asPath } = this.props.router;

    const haveSuccess = successRegexp.exec(asPath);
    const haveError = errorRegexp.exec(asPath);
    const haveSuccessWithdraw = successWithdrawRegexp.exec(asPath);
    const haveMobileTrading = mobileTradingRegexp.exec(asPath);
    // const haveError = asPath.substr(asPath.length - 5) === '#error';

    // console.log('haveSuccess', haveSuccess);

    if (haveSuccessWithdraw) {
      this.props.router.replace(asPath, asPath.replace(successWithdrawRegexp, ''), { shallow: true });
      this.props.showWithdrawSuccess();
    }

    // cleanup params
    if (haveSuccess) {
      this.props.router.replace(asPath, asPath.replace(successRegexp, ''), { shallow: true });
      this.props.showSuccess();
    }

    if (haveError) {
      this.props.router.replace(asPath, asPath.replace(errorRegexp, ''), { shallow: true });
      this.props.showError();
    }

    if (haveMobileTrading) {
      this.props.router.replace(asPath, asPath.replace(mobileTradingRegexp, ''), { shallow: true });
      this.props.showMobileTrading();
    }
  }

  checkIfDepositModalNeeded() {
    const hash = window.location.hash ? window.location.hash.substr(1) : null;

    if (hash == 'deposit') {
      this.props.openDepositModal();
    }
  }

  componentWillUnmount() {
    this.props.stopWaitForAssetReturnsChanges();

    if (this.props.hasBalancesChanges) {
      this.props.stopWaitForChangeCurrency();
    }

    this.props.stopListenBalanceChange();

    this.props.closeModal();

    this.source && this.source.cancel();

    window.removeEventListener('resize', this.recalculateSparklines);
  }

  handleTrade = (from, to) => {
    if (isMobile()) {
      this.props.showMobileTrading();
      return;
    }

    this.source = redirectToExchange({
      from,
      to,
    }, () => {
      this.source = null;
    });
  }

  render() {
    const chartState = chartMachine.stateFromProps(this.props);
    const isInitialLoading = chartState === null || (this.props.chart === null && chartState === 'inProgress');
    const isErrorLoading = chartState === 'error';

    return (
      <div className="portfolio-dashboard">
        <div className="dashboard-table__total-row">
          <div className="index__container">
            <Total
              balance={ balanceMachine.getDataFromProps(this.props) }
              balanceRequestStatus={ balanceMachine.getStateFromProps(this.props) }
              onDepositClick={ this.props.openDepositModal }
              onWithdrawClick={ this.props.openWithdrawModal }
              kycPassed={ this.props.kycPassed }
              canDepositWithdraw
            />
            {
              this.props.hasBalancesChanges &&
              <div className="dashboard-chart-filter">
                <div>
                  <Switch
                    value={ this.props.chartFilter }
                    items={ CHART_OPTIONS }
                    onChange={ this.props.changeChartFilter }
                  />
                  {
                    chartState === 'inProgress' && !isInitialLoading && <RectSpinner className="white" small inline/>
                  }
                </div>
              </div>
            }
          </div>
          {
            this.props.hasBalancesChanges &&
            <div className="dashboard-chart-block">
              <Chart
                values={ this.props.chart }
                currency={ this.props.chartCurrency }
                styles={ CHART_STYLES }
                isInitialLoading={ isInitialLoading }
                isErrorLoading={ isErrorLoading }
              />
            </div>
          }
          <div className="index__container">
            <div className="dashboard-table-block__tabs-container">
              <div className="dashboard-table-block__tabs">
                <TradeTabs tabs={ MAIN_TABS } active={ this.props.mainTab } onChange={ this.props.changeMainTab } />
              </div>
              <div className="dashboard-table-block__tabs dashboard-table-block__tabs--blue">
                <TradeTabs tabs={ SUB_TABS } active={ this.props.subTab } onChange={ this.props.changeSubTab } />
              </div>
            </div>
          </div>
        </div>
        <div className="dashboard-table-block">
          <div className="index__container">
            {
              this.props.mainTab === 'balances' &&
              <AssetsTable
                assetsList={ this.props.assetsList }
                userData={ this.props.userData }
                onTrade={ this.handleTrade }
                onClickRow={ this.props.setClickedRow }
                onClearClickRow={ this.props.clearClickedRow }
                rowClicked={ this.props.rowClicked }
                kycPassed={ this.props.kycPassed }
                sortBy={ this.props.balancesSortBy }
                sortDirection={ this.props.balancesSortDirection }
                onSortClick={ this.props.changeSort }
                search={ this.props.balancesSearch }
                onChangeSearch={ this.props.changeBalancesSearch }
                onTableClick={ this.props.clickOnTable }
                onTradeClose={ this.props.closeTrade }
                tradeAsset={ this.props.clickedTradeOnAsset }
                clickedTradeOptions={ this.props.clickedTradeOptions }
                onSetHideSmallAmounts={ this.props.setHideSmallAmounts }
                hideSmallAmounts={ this.props.hideSmallAmounts }
              />
            }
            {
              this.props.mainTab === 'markets' &&
              <TradePairs
                sparklinesWidth={ this.state.bigSparklineWidth }
                assets={ this.props.plainAssets }
                returns={ this.props.pricesReturns }
                sparklines={ this.props.sparklines }
                volumes={ this.props.volumes }
                tab={ this.props.subTab }
                sort={ this.props.marketsSortBy }
                sortDirection={ this.props.marketsSortDirection }
                onSort={ this.props.changeSort }
              />
            }
          </div>
        </div>

        <div className="dashboard__gray-block">
          <div className="dashboard__gray-block__title">
            Top Growth Products
          </div>
          <div className="dashboard__top-growth-slider">
            <div className="index__container">
              <Swiper { ...SWIPER_OPTIONS }>
                {
                  this.props.topGrowth.map(asset => (
                    <ShowcaseProduct
                      key={ asset.id }
                      asset={ asset }
                    />
                  ))
                }
              </Swiper>
            </div>
          </div>
          <div className="dashboard__top-growth-button">
            <Link route="/products" className="button dashboard__link-button">All products</Link>
          </div>
        </div>

        <VbOperationsContainer
          modalData={ this.props.modalData }
          modalBalance={ this.props.modalBalance }
          walletOptions={ this.props.convertableAssetsOptions }
          walletBalances={ this.props.walletBalances }
          onSelectSellAmountPercent={ this.props.selectAmountPercent }
        />
        <ConfirmDialog />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DashboardPage));
