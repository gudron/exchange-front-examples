import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AssetsTableRow from '~/components/dashboard/AssetsTableRow';
import SortableCell from '~/components/dashboard/SortableCell';
import FlyingSelect from '~/components/FlyingSelect';
import TradeSearch from '~/components/trade-pairs/TradeSearch';
import Checkbox from '~/components/Checkbox';
import TradeTableBodyNotFound from '~/components/trade-pairs/TradeTableBodyNotFound';
import { isLaptop } from '~/helpers/adaptive';
import { getCoords, getElementWidth, getHeight } from '~/helpers/measures';
import { findAncestor, lookupForDataAttribute } from '~/helpers/dom';

import { ASSETS_TABLE_COLUMNS } from '~/redux/selectors/assetsList';

import './AssetsTable.sass';

export default class AssetsTable extends Component {
  static propTypes = {
    assetsList: PropTypes.array.isRequired,
    convertedSuccess: PropTypes.bool,
    userData: PropTypes.object.isRequired,
    onTrade: PropTypes.func.isRequired,
    onClickRow: PropTypes.func.isRequired,
    onClearClickRow: PropTypes.func.isRequired,
    rowClicked: PropTypes.string,
    kycPassed: PropTypes.bool.isRequired,
    sortBy: PropTypes.string,
    sortDirection: PropTypes.string,
    onSortClick: PropTypes.func.isRequired,
    search: PropTypes.string.isRequired,
    onChangeSearch: PropTypes.func.isRequired,
    onTableClick: PropTypes.func.isRequired,
    tradeAsset: PropTypes.string,
    onTradeClose: PropTypes.func.isRequired,
    clickedTradeOptions: PropTypes.array,
    onSetHideSmallAmounts: PropTypes.func.isRequired,
    hideSmallAmounts: PropTypes.bool.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      tradeTop: 0,
      tradeLeft: 0,
      tradeWidth: 0,
    };
  }

  handleRowClick = (assetId) => {
    if (isLaptop()) {
      return;
    }

    if (this.props.rowClicked === assetId) {
      this.props.onClearClickRow();

      return;
    }

    this.props.onClickRow(assetId);
  }

  renderAsset = (asset) => {
    return (
      <AssetsTableRow
        key={ asset.name }
        asset={ asset }
        userData={ this.props.userData }
        onRowClick={ this.handleRowClick }
        isClicked={ asset.routingKey ? this.props.rowClicked === asset.routingKey : this.props.rowClicked === asset.name }
        isLock={ asset.kycOnly && !this.props.kycPassed }
      />
    );
  }

  handleSortClick(name) {
    this.props.onSortClick(name);
  }

  handleCellsClick = (e) => {
    const foundAttributes = lookupForDataAttribute(e.target, ['symbol', 'action'], 'assets-table__body');
    if (foundAttributes) {
      e.stopPropagation();
    }

    if (!foundAttributes) {
      return;
    }

    if (foundAttributes.action === 'trade') {
      const foundAsset = this.props.assetsList.find(item => item.name === foundAttributes.symbol);

      if (foundAsset === undefined || !foundAsset.isShowTradeButton || !foundAsset.canTrade || !foundAsset.needKycForTrade) {
        return;
      }

      const actionEl = e.target.classList.contains('assets-table__cell') ? e.target : findAncestor(e.target, 'assets-table__cell');
      const coords = getCoords(actionEl);
      const width = getElementWidth(actionEl);
      const height = getHeight(actionEl);
      this.setState({
        tradeLeft: coords.left,
        tradeTop: coords.top + height - 20,
        tradeWidth: width,
        tradeAsset: foundAttributes.symbol,
      });
    }

    this.props.onTableClick(foundAttributes);
  }

  handleTradeSelect = (toAsset) => {
    this.props.onTrade(this.state.tradeAsset, toAsset);
  }

  render() {
    const needShowEmptyPlaceholder = this.props.assetsList.length === 0 && this.props.search !== '';

    return (
      <React.Fragment>
        <div className="assets-table__search-line">
          <TradeSearch
            value={ this.props.search }
            onChange={ this.props.onChangeSearch }
            mobileShowing={ false }
            onShow={ this.showMobileSearch }
            onHide={ this.hideMobileSearch }
          />
          <div className="assets-table__hide-small-amounts">
            <Checkbox onChange={ this.props.onSetHideSmallAmounts } value={ this.props.hideSmallAmounts }>Hide small amounts</Checkbox>
          </div>
        </div>
        <div className="assets-table">
          <div className="assets-table__colgroup">
            <div className="assets-table__col assets-table__col--name"></div>
            <div className="assets-table__col assets-table__col--total"></div>
            <div className="assets-table__col assets-table__col--change"></div>
            <div className="assets-table__col assets-table__col--price"></div>
            <div className="assets-table__col assets-table__col--icon"></div>
            <div className="assets-table__col assets-table__col--icon"></div>
            <div className="assets-table__col assets-table__col--icon"></div>
          </div>
          <div className="assets-table__header">
            <div className="assets-table__row">
              {
                ASSETS_TABLE_COLUMNS.map((column, index) => (
                  <SortableCell
                    key={ index }
                    sortDirection={
                      column.sortable
                        ? (
                          this.props.sortBy === column.key
                            ? this.props.sortDirection
                            : null
                        )
                        : null
                    }
                    className={ column.className }
                    onClick={ column.sortable ? this.handleSortClick.bind(this, column.key) : null }
                  >{ column.title }{ column.afterTitle && <span className="assets-table__cell--thin"> { column.afterTitle }</span> }</SortableCell>
                ))
              }
            </div>
          </div>
          <div className="assets-table__body" onClick={ this.handleCellsClick }>
            {
              this.props.assetsList.map(this.renderAsset)
            }
          </div>
        </div>
        { needShowEmptyPlaceholder && <TradeTableBodyNotFound /> }
        {
          this.props.tradeAsset !== null &&
          <FlyingSelect
            left={ this.state.tradeLeft }
            top={ this.state.tradeTop }
            width={ this.state.tradeWidth }
            onOutsideClick={ this.props.onTradeClose }
            onSelect={ this.handleTradeSelect }
            options={ this.props.clickedTradeOptions }
          />
        }
      </React.Fragment>
    );
  }
}
