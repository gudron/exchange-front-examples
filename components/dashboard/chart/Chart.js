import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import format from 'date-fns/format';
import { differenceInMinutes } from 'date-fns';
import memoize from "memoize-one";
import { formatMoney, formatCurrency } from "~/helpers/newMoney";
import BigNumber from 'bignumber.js';
import { scaleUtc } from 'd3-scale';
import { utcWeek, utcSecond, utcMinute, utcHour, utcDay, utcMonth, utcYear } from 'd3-time';
import { utcFormat } from "d3-time-format";
import _ from 'lodash';
import RectSpinner from '~/components/RectSpinner';

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Text } from 'recharts';

import './Chart.sass';

var formatMillisecond = utcFormat(".%L"),
  formatSecond = utcFormat(":%S"),
  formatMinute = utcFormat("%I:%M"),
  formatHour = utcFormat("%H:00"),
  formatDay = utcFormat("%e %b"),
  formatWeek = utcFormat("%b %d"),
  formatMonth = utcFormat("1 %b"),
  formatYear = utcFormat("%Y");

function multiFormat(date) {
  return (utcSecond(date) < date ? formatMillisecond
    : utcMinute(date) < date ? formatSecond
      : utcHour(date) < date ? formatMinute
        : utcDay(date) < date ? formatHour
          : utcMonth(date) < date ? formatDay
            : utcYear(date) < date ? formatMonth
              : formatYear)(date);
}

const userTimezoneOffset = new Date().getTimezoneOffset() * 60 * 1000;

function renderChangesRows(changes) {
  changes = changes.map(item => ({
    amount: new BigNumber(item.amount),
    asset: item.asset,
    assetFormatted: formatCurrency(item.asset),
  }));

  return <React.Fragment>{ changes.map((item, index) => (
    <p
      key={ index }
      className={ classnames('desc', {
        sent: item.amount.isNegative(),
        receive: item.amount.isPositive(),
      }) }
    >
      { item.assetFormatted } { formatMoney({
        amount: item.amount.abs(),
        currency: item.asset,
      }) } { item.amount.isNegative() ? 'Sent' : 'Received' }
    </p>
  )) }</React.Fragment>;
}

const generateCustomTooltip = memoize((currency) => {
  const currencyFormatted = formatCurrency(currency);

  return function CustomTooltip({ active, payload, label }) {
    if (active) {
      const data = payload[0];
      const dataPayload = data.payload;
      const date = label;
      const textDate = format(
        new Date(date + userTimezoneOffset),
        'MMM D, YY (HH:mm)'
      );

      return (
        <div className="chart-tooltip">
          <p className="value">{ currencyFormatted } { formatMoney({ amount: data.value, currency }) }  <span className="date">{ textDate }</span></p>
          {
            dataPayload.changes && renderChangesRows(dataPayload.changes)
          }
        </div>
      );
    }

    return null;
  };
});

const formatCurrencyValue = (value, currency) => {
  const v = parseFloat(value);

  if (v === 0) return '0';

  if (v >= 1000000) return `${ formatMoney({ amount: v / 1000000, currency }, false, true) }M`;
  if (v > 1000) return `${ formatMoney({ amount: v / 1000, currency }, false, true) }k`;

  return formatMoney({ amount: v, currency }, false, true);
};

const yTickFormatter = memoize((values, currency) => {
  const currencyFormatted = formatCurrency(currency);

  const cached = {};

  const preparer = y => parseFloat(y) === 0 ? '0' : (currencyFormatted + ' ' + formatCurrencyValue(y, currency));

  values.forEach(value => {
    cached[value.y] = preparer(value.y);
  });

  return (y) => {
    if (cached[y] === undefined) {
      cached[y] = preparer(y);
    }

    return cached[y];
  };
});

const xTickFormatter = (timestamp) => {
  return multiFormat(new Date(timestamp));
};

const generateTicks = memoize(values => {
  // todo get first and last values becasue ordered
  const ticks = scaleUtc().domain([_.minBy(values, 'x').x, _.maxBy(values, 'x').x]).nice().ticks();

  return ticks.map(tick => +tick);
});

const prepareData = memoize(values => {
  let currentIndex = 0;

  return values.map((value, index) => {
    let returnValue = value;

    if ((value.receive || value.sent) && currentIndex === 1) {
      const { sent, receive, ...rest } = value;
      const newValue = {
        ...rest,
      };

      if (sent) {
        newValue.sent2 = sent;
      }

      if (receive) {
        newValue.receive2 = receive;
      }

      returnValue = newValue;
    }

    if (value.descr) {
      currentIndex = (currentIndex + 1) % 2;
    }

    returnValue.y = parseFloat(returnValue.y);

    return returnValue;
  });
});

const prepareXAxisDomain = memoize(values => {
  if (values.length === 2 && Math.abs(differenceInMinutes(new Date(values[0].x), new Date(values[1].x))) < 1) {
    return [dataMin => dataMin - 24*60*60*1000, dataMax => dataMax + 24*60*60*1000];
  }

  return ['auto', 'auto'];
});


export default class Chart extends Component {
  static propTypes = {
    values: PropTypes.array.isRequired,
    currency: PropTypes.string.isRequired,
    styles: PropTypes.object.isRequired,
    isInitialLoading: PropTypes.bool,
    isErrorLoading: PropTypes.bool,
  }

  render() {
    if (this.props.isInitialLoading) {
      return <div className="chart__no-data-splash"><RectSpinner className="white" /></div>;
    }

    if (this.props.isErrorLoading) {
      return <div className="chart__no-data-splash">Error</div>;
    }

    if (this.props.values === null || this.props.values.length === 0) {
      return <div className="chart__no-data-splash">No data</div>;
    }

    const data = prepareData(this.props.values);
    const yfmt = yTickFormatter(this.props.values, this.props.currency);
    const { styles } = this.props;

    return (
      <ResponsiveContainer width="100%" height={ 300 }>
        <LineChart
          data={ data }
          margin={ { top: 50, right: 30, left: 0, bottom: 5 } }
          isAnimationActive={ false }
        >
          <CartesianGrid { ...styles.gridStyle } />
          <XAxis
            dataKey="x"
            axisLine={ false }
            tickLine={ styles.tickLine }
            padding={ { left: 30, right: 30 } }
            tickFormatter={ xTickFormatter }
            scale="time"
            domain={ prepareXAxisDomain(this.props.values) }
            type="number"
            ticks={ generateTicks(this.props.values) }
            tick={ styles.tickStyle }
          />
          <YAxis
            axisLine={ false }
            tickLine={ false }
            domain={ ['dataMin', 'dataMax'] }
            tick={ props => <Text style={ styles.tickStyle } { ...props } >{ yfmt(props.payload.value) }</Text> }
          />
          <Tooltip
            isAnimationActive={ false }
            position={ { y: 10 } }
            cursorStyle={ { stroke: '#000' } }
            content={ generateCustomTooltip(this.props.currency) }
          />
          <Line type="linear" dataKey="y" stroke={ styles.mainLineColor } strokeWidth="2" dot={ false } isAnimationActive={ false } />
          <Line type="linear" dataKey="receive" stroke={ styles.receiveLineColor }  strokeWidth="2" dot={ false } isAnimationActive={ false } />
          <Line type="linear" dataKey="receive2" stroke={ styles.receiveLineColor } strokeWidth="2" dot={ false } isAnimationActive={ false } />
          <Line type="linear" dataKey="sent" stroke={ styles.sentLineColor } strokeWidth="2" dot={ false } isAnimationActive={ false } />
          <Line type="linear" dataKey="sent2" stroke={ styles.sentLineColor } strokeWidth="2" dot={ false } isAnimationActive={ false } />
        </LineChart>
      </ResponsiveContainer>
    );
  }
}
