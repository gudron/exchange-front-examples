import React from 'react';
import classnames from 'classnames';

const SortableCell = ({ sortDirection, children, onClick, className }) => {
  return (
    <div className={ classnames(className, 'assets-table__cell', {
      'assets-table__cell--clickable': !!onClick,
    }) } onClick={ onClick }>
      <div className={ classnames({
        'order-direction order-direction--up': sortDirection === 'up',
        'order-direction order-direction--down': sortDirection === 'down',
      }) }>
        { children }
      </div>
    </div>
  );
};

export default SortableCell;
