#! /bin/bash

BABEL_ENV=panel babel --extensions ".tsx,.js" --watch components --out-dir lib/js/components
# BABEL_ENV=panel babel --extensions ".tsx,.js" --watch helpers --out-dir lib/js/helpers
# BABEL_ENV=panel babel --extensions ".tsx,.js" --watch services --out-dir lib/js/services
