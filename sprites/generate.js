const path = require('path');
const { writeFileSync } = require('fs');
const createSprite = require('svg-mixer');
const postcssMixer = require('postcss-svg-mixer');

const outputRelativePath = '/static/next/i/sprites/';
const outputCssPath = '../components/trade-pairs/icons.css';
const input = path.resolve(__dirname, '../static/next/i/sprites/color-assets/*.svg');
const output = path.resolve(__dirname, '../' + outputRelativePath);

// Create sprite programmatically
createSprite(input).then(result => {
  writeFileSync(path.resolve(output, result.filename), result.content);

  /**
   * Generate CSS code like
   * .img1 {background: url(img/img1.svg);}
   * .img2 {background: url(img/img2.svg);}
   */
  const cssInput = result.sprite.symbols
    .map(s => `.${s.id} {background: url(${ s.image.path });}`)
    .join('\n');

  postcssMixer
    .process(cssInput, { from: __dirname }, { userSprite: result.sprite })
    .then(({ css }) => writeFileSync(path.resolve(__dirname, outputCssPath), css.replace(/sprite\.svg/g, outputRelativePath + 'sprite.svg')));
});
