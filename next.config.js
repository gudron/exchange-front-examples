const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { ANALYZE } = process.env;
const webpack = require('webpack');

const withSass = require('@zeit/next-sass');
const withSourceMaps = require('@zeit/next-source-maps')()

const fileLoaderExclude = [/\.js$/, /\.html$/, /\.json$/, /\.s(a|c)ss$/, /\.css$/, /\.svg$/, /\.(ts|tsx|js|jsx|mjs)$/];

const ignorePlugin = new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/);

function prepareProdConfig(config, isServer) {
  config.module.rules.push(
    // {
    //   test: /\.(svg)/,
    //   loader: 'emit-file-loader',
    //   options: {
    //     name: 'dist/[path][name].[ext]'
    //   }
    // },
    {
      test: /\.svg$/,
      exclude: /node_modules/,
      use: [
        {
          loader: 'svg-sprite-loader',
          // options: {
          //   extract: true,
          //   spriteFilename: 'static/media/[name].[chunkname].[ext]',
          // },
        },
        // 'svg-fill-loader',
      ],
    },
    {
      exclude: fileLoaderExclude,
      loader: require.resolve('file-loader'),
      options: {
        publicPath: '/',
        name: 'static/media/[name].[hash:8].[ext]',
      },
    },
  );

  if (!isServer) {
    const originalEntry = config.entry;
    config.entry = async function () {
      const entries = await originalEntry();
      if (entries['main.js']) {
        entries['main.js'].unshift(require.resolve('./polyfills'));
      }

      return entries;
    };
  }

  if (ANALYZE) {
    config.plugins.push(new BundleAnalyzerPlugin({
      analyzerMode: 'server',
      analyzerPort: isServer ? 8888 : 8889,
      openAnalyzer: true
    }))
  }

  config.plugins.push(ignorePlugin);

  return config;
}

function prepareDevConfig(config, isServer) {
  config.module.rules.push(
    {
      test: /\.svg$/,
      exclude: /node_modules/,
      use: [
        'svg-sprite-loader',
        // 'svg-fill-loader',
        // 'svgo-loader',
      ],
    }
  );

  config.module.rules.push(
    {
      // Exclude `js` files to keep "css" loader working as it injects
      // it's runtime that would otherwise processed through "file" loader.
      // Also exclude `html` and `json` extensions so they get processed
      // by webpacks internal loaders.
      exclude: fileLoaderExclude,
      loader: require.resolve('file-loader'),
      options: {
        name: '[name].[hash:8].[ext]',
        outputPath: 'static/media/',
        publicPath: '/_next/',
      },
    },
  );

  config.plugins.push(ignorePlugin);

  return config;
}

module.exports = withSourceMaps(
  withSass({
    publicRuntimeConfig: {
      sentryDsn: process.env.SENTRY_PUBLIC_DSN,
    },
    webpack: (config, { dev, isServer, buildId }) => {
      let preparedConfig;

      if (dev) {
        preparedConfig = prepareDevConfig(config, isServer);
      } else {
        preparedConfig = prepareProdConfig(config, isServer);
      }

      // setup sentry
      preparedConfig.plugins.push(
        new webpack.DefinePlugin({
          'process.env.SENTRY_RELEASE': JSON.stringify(buildId),
        })
      );

      if (!isServer) {
        preparedConfig.resolve.alias['@sentry/node'] = '@sentry/browser';
      }

      return preparedConfig;
    },
  })
);

