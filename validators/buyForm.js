import Joi from 'joi-browser';


// import { stringAmountValidator } from '~/helpers/stringAmountValidator';
// TODO: map of decimals for assets
// import { TOKEN_DECIMALS } from '~/helpers/decimals';

import { getSymbolSettings } from '~/validators/digits';
import { prepareDigitsValidator } from '~/validators/validator';

export const BUY_CURRENCY_FIELD = 'sellCurrency';
export const BUY_AMOUNT_FIELD = 'buyAmount';

// export const amountValidator = stringAmountValidator.amount().valid().precision(TOKEN_DECIMALS).greater(0);

const MAX_AMOUNT_ERROR_TEXT = 'Insufficient funds on balance';

// // this it pre self-submit validator. Used by dashboard saga
// import { BUY_AMOUNT_FIELD, BUY_CURRENCY_FIELD } from '~/redux/selectors/dashboard';

export const staticValidator = (formValues, assetId, digits) => {
  if (!formValues || formValues[BUY_AMOUNT_FIELD] === undefined || formValues[BUY_AMOUNT_FIELD] === '') {
    return null;
  }

  const buyCurrency = formValues[BUY_CURRENCY_FIELD];
  const symbolSettings = getSymbolSettings(assetId, buyCurrency, digits);

  const validateResult = Joi.validate(
    formValues[BUY_AMOUNT_FIELD],
    prepareDigitsValidator(symbolSettings, MAX_AMOUNT_ERROR_TEXT)
    // amountValidator.error(buyAmountErrors)
  );

  if (!validateResult || !validateResult.error) {
    return null;
  }

  return {
    message: validateResult.error.message,
  };
};

// TODO: fix this code
// export const afterExpectedPriceValidator = (expectedPrice, walletCurrencies) => {
//   // if (expectedPriceRate !== 'success') {
//   //   return {
//   //     amount: {
//   //       message: 'Please wait for rate loaded',
//   //     },
//   //   };
//   // }

//   // const { currency } = formValues;
//   const currency = expectedPrice.currency;

//   const walletCurrency = walletCurrencies[currency];
//   const balanceInSelectedCurrency = new BigNumber(walletCurrency.balance);
//   // const maxTokensInSelectedCurrency = new BigNumber(expectedPrice.amount.toString());
//   // const maxTokensInSelectedCurrency = balanceInSelectedCurrency.dividedBy(rate);

//   const validateResult = Joi.validate(
//     expectedPrice.amount.toString(),
//     amountValidator.error(buyAmountErrors).max(balanceInSelectedCurrency)
//   );

//   if (!validateResult || !validateResult.error) {
//     return null;
//   }

//   return {
//     [BUY_AMOUNT_FIELD]: {
//       message: validateResult.error.message,
//     },
//   };
// };
