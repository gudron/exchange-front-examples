import _ from 'lodash';

function prepareKey(from, to) {
  return `${ _.toUpper(from) }_${ _.toUpper(to) }`;
}

export const getDigitsByAssets = (from, to, digits) => {
  return _.get(digits, [prepareKey(from, to), 'digits'], 8);
};

export const getPrecision= (from, to, digits) => {
  const defaultMin = 8 - getDigitsByAssets(from, to, digits);

  return _.get(digits, [prepareKey(from, to), 'precision'], defaultMin);
};

export const getSymbolSettings = (from, to, digits) => {
  return _.get(digits, prepareKey(from, to));
}
