import memoize from 'memoize-one';
import { stringAmountValidator } from '~/helpers/stringAmountValidator';

function prepareErrors(minValue, textAboutMaxValueError) {
  const amountErrors = (errors) => {
    const error = errors[0]; // use only first error because it only one can throw

    switch (error.type) {
      case 'amount.valid':
        return 'Value must be valid number';
      case 'amount.greater':
        return `Value must be greater than zero`;
      case 'amount.max':
        return textAboutMaxValueError;
      case 'amount.multiple':
        return 'Volume must be multiple of ' + minValue;
      default:
        return 'Invalid value';
    }
  };

  return amountErrors;
}

export const prepareDigitsValidator = memoize((symbolSettings, textAboutMaxValueError) => {
  return stringAmountValidator
    .amount()
    .valid()
    .greater(0)
    .max(symbolSettings.maxVolume)
    .multiple(symbolSettings.minVolume)
    .error(prepareErrors(symbolSettings.minVolume, textAboutMaxValueError));
});
