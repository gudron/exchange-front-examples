import Joi from 'joi-browser';
// import BigNumber from 'bignumber.js';

import { getSymbolSettings } from '~/validators/digits';
import { prepareDigitsValidator } from '~/validators/validator';

export const SELL_CURRENCY_FIELD = 'buyCurrency';
export const SELL_AMOUNT_FIELD = 'sellAmount';

const MAX_AMOUNT_ERROR_TEXT = 'Insufficient funds on balance';

// // this it pre self-submit validator. Used by dashboard saga
export const staticValidator = (formValues, assetId, digits) => {
  if (!formValues || formValues[SELL_AMOUNT_FIELD] === undefined || formValues[SELL_AMOUNT_FIELD] === '') {
    return null;
  }

  const sellCurrency = formValues[SELL_CURRENCY_FIELD];
  const symbolSettings = getSymbolSettings(assetId, sellCurrency, digits);

  const validateResult = Joi.validate(
    formValues[SELL_AMOUNT_FIELD],
    prepareDigitsValidator(symbolSettings, MAX_AMOUNT_ERROR_TEXT)
    // amountValidator.error(sellAmountErrors)
  );

  if (!validateResult || !validateResult.error) {
    return null;
  }

  return {
    message: validateResult.error.message,
  };
};

// TODO: fix that price
// export const afterExpectedPriceValidator = (expectedPrice, walletCurrencies) => {
//   // if (expectedPriceRate !== 'success') {
//   //   return {
//   //     amount: {
//   //       message: 'Please wait for rate loaded',
//   //     },
//   //   };
//   // }

//   // const { currency } = formValues;
//   const currency = expectedPrice.currency;

//   const walletCurrency = walletCurrencies[currency];
//   const balanceInSelectedCurrency = new BigNumber(walletCurrency.balance);
//   // const maxTokensInSelectedCurrency = new BigNumber(expectedPrice.amount.toString());
//   // const maxTokensInSelectedCurrency = balanceInSelectedCurrency.dividedBy(rate);

//   const validateResult = Joi.validate(
//     expectedPrice.amount.toString(),
//     amountValidator.error(buyAmountErrors).max(balanceInSelectedCurrency)
//   );

//   if (!validateResult || !validateResult.error) {
//     return null;
//   }

//   return {
//     [BUY_AMOUNT_FIELD]: {
//       message: validateResult.error.message,
//     },
//   };
// };
