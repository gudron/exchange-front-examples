import { Required } from 'plag-validator/lib/rules/any/Required';
import { NotRequired } from 'plag-validator/lib/rules/any/NotRequired';
import { Allow } from 'plag-validator/lib/rules/any/Allow';
import { MinLength } from 'plag-validator/lib/rules/string/MinLength';
import { MaxLength } from 'plag-validator/lib/rules/string/MaxLength';
import { Pattern } from 'plag-validator/lib/rules/string/Pattern';
import { MinCount } from 'plag-validator/lib/rules/array/MinCount';
import { MinYears } from 'plag-validator/lib/rules/date/MinYears';

export const PERSON_INFORMATION_STEP_ID = 1;
export const DOCUMENTS_STEP_ID = 2;

const datePattern = new Pattern(/^\d{4}-\d{2}-\d{2}$/, 'Must be valid date');

export const newFieldsValidator = {
  firstName: [new Required('First name is required')],
  lastName: [new Required('Last name is required')],
  birthday: [new Required('Birthday required'), datePattern, new MinYears(18, 'You should be 18 years old')],
  residenceCountry: [new Required('Residence country is required'), new MinLength(2, 'Residence country is required')],
  residenceCity: [new Required('Residence city is required'), new MaxLength(200, 'Residence city is required')],
  residenceAddress: [new Required('Residence address is required'), new MaxLength(200, 'Residence address is required')],
  residenceZip: [new Required('Residence zip is required'), new MaxLength(50, 'Residence zip is required')],
  passportScan: [new Required('Passport scan is required')],
  idType: [new Required('Please, select identity type'), new Allow(['passport', 'id'], 'Please, select identity type')],
  idFrontScan: [new Required('ID front scan is required')],
  idBackScan: [new Required('ID back scan is required')],
  bankStatementScan: [new Required('Bill scan is required')],
  actAsCompany: [new Allow(['true', 'false', undefined, null, false], 'Please, select you act as')],
  companyName: [new Required('Company name is required'), new MaxLength(500, 'Company name is required')],
  companyRegistrationNumber: [new Required('Company registration number is required'), new MaxLength(200, 'Company registration number is required')],
  companyCountry: [new Required('Company country is required'), new MinLength(2, 'Company country is required')],
  companyLegalAddress: [new Required('Company address is required'), new MaxLength(500, 'Company address is required')],
  companyConfirmRepresentative: [new Required('Please confirm company representative'), new Allow(['true'], 'Please confirm company representative')],
  companyDocuments: [new Required('Please upload some files'), new MinCount(1, 'Please upload some files')],
  sourceOfFunds: [new Required('Source of funds is required'), new MaxLength(500, 'Source of funds is required')],
};

export const stepsFields = {
  [PERSON_INFORMATION_STEP_ID]: function (fields, userData) {
    return [
      'firstName',
      'lastName',
      'birthday',
      'residenceCountry',
      'residenceCity',
      'residenceAddress',
      'residenceZip',
    ];
  },
  [DOCUMENTS_STEP_ID]: function (fields, userData) {
    const commonFields = ['actAsCompany', 'idType', 'bankStatementScan'];

    const changableFiels = (fields.idType === 'passport')
      ? ['passportScan']
      : ['idFrontScan', 'idBackScan'];

    let companyFields = [];

    if (fields.actAsCompany === 'true') {
      companyFields = [
        'companyName',
        'companyRegistrationNumber',
        'companyCountry',
        'companyLegalAddress',
        'companyConfirmRepresentative',
        'companyDocuments',
        'sourceOfFunds',
      ];
    }

    return [...changableFiels, ...commonFields, ...companyFields];
  },
};
